package com.ecommerce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.User;
import com.ecommerce.model.UserInfo;

@Controller
@RequestMapping(value="/recoverPasswordController")
public class RecoverPassword {
	
	//display the recover password field
	@RequestMapping(value="/recover")
	public ModelAndView getRecoverPassForm() {
		ModelAndView modelAndView = new ModelAndView("/Generic/RecoverPassword");
		
		//to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		return modelAndView;
	}
	
	/*
	 * 
	*/
	@RequestMapping(value="/recoverComplete.html")
	public ModelAndView mainPage(HttpServletRequest request) throws IOException {
		ModelAndView modelAndView;
		
		UserInfo userInfo = new UserInfo();	//this is current new user, will be saved in the database
		User user = new User();	//not available in our recent version
		
		List<UserInfo> userDatabase = null;	// all user info goes here
		
		//fetch database to get all the user info
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			userDatabase = session.createQuery("from UserInfo").list();	//get all the usre info
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//store the name, passwords, mail and cell number
		String userName = request.getParameter("userName");
		String fullName =  request.getParameter("userFullName");
		String userEmail = request.getParameter("userEmail");
		String userMobileNumber = request.getParameter("userMobileNumber");
		String userPassword = request.getParameter("userPassword");
		String userConformPassword = request.getParameter("userConfirmPassword");
		
		
		boolean errorFlag = true;	//let's assume no error exist
		String errorLog = "";
		
		System.out.println("uN: " + userName + "\nfN: " + fullName + "\nEmail: " + userEmail + "\nMobile: " + userMobileNumber);
		
		for(UserInfo ui : userDatabase) {
			try {
				if(ui.getUserEmail().equals(userEmail) == true && ui.getUserName().equals(userName) == true 
						&& ui.getUserFullName().equals(fullName) == true && ui.getUserMobileNumber().equals(userMobileNumber) == true ) {
					System.out.println("------------valid user");
					errorFlag = false;
					userInfo = ui;	//got a valid user
					break;	//updating information
				}
				
			} catch(Exception e) {
				errorFlag = true;
				System.out.println("Exception on database value: encounter null values");
			}
			
		}
		
		if(errorFlag) {
			errorLog += "<br/>* Not a valid User.";
		}
		
		//checking password
		if(errorFlag == false) {
			if(userPassword.length() < 6) {
				errorLog += "<br/>* Password must be at least 6 characters.";
				errorFlag = true;
			}
			//matching password
			if(!userPassword.equals(userConformPassword)) {
				errorLog += "<br/>* Password does not match";
				errorFlag = true;	
			}
		}
		
		if(errorFlag == false) {
			modelAndView = new ModelAndView("/Generic/LogInForm");
			
			userInfo.setUserPassword(userPassword);
			
			//update the userInfo
			try {
				SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
				org.hibernate.Session session = sessionFactory.openSession();
				
				session.update(userInfo);	//updating the user Info
				
				session.beginTransaction();
				session.close();		
			} catch(Exception e) {
				System.out.println("Exception on fetching database: " + e.toString());
			}
			
			modelAndView.addObject("errorLog", errorLog);
			return modelAndView;
		} else {
			modelAndView = new ModelAndView("/Generic/Registration");
			modelAndView.addObject("errorLog", errorLog);
			return modelAndView;
		}
	}

}
