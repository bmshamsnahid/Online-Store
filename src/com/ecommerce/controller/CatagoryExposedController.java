package com.ecommerce.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;

@Controller
@RequestMapping(value="/CatagoryController")
public class CatagoryExposedController extends HttpServlet {
	
	@RequestMapping("/Exposed")
	public ModelAndView exposedCatagory(HttpServletRequest request) {
		ModelAndView modelAndView;
		
		
		try {
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				System.out.println("User Logged in");
				System.out.println("Name: " + request.getSession().getAttribute("userName"));
				modelAndView = new ModelAndView("/User/CatagoryExposed");
			} else {
				System.out.println("No name. Logged Out");
				modelAndView = new ModelAndView("/Guest/CatagoryExposed");
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			modelAndView = new ModelAndView("Guest/MainPage");
		}
		
		
		System.out.println("This is a category exposed controller.");
		
		String productCatagory = request.getParameter("productCatagory");
		System.out.println("Catagory: " + productCatagory);
		
		List<ProductInformation> productDatabase = null;
		ArrayList<ProductInformation> productCatagoryItemList = new ArrayList<>();
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return new ModelAndView("Guest/MainPage");
		}	
		
		for(ProductInformation productInfo : productDatabase) {
			String catagory = productInfo.getProductCatagory();
			System.out.println("Name: " + productInfo.getProductCatagory());
			System.out.println("Category: " + productInfo.getProductCatagory());
			System.out.println("Size of rc category: " + catagory.length());
			System.out.println("Size of parameter category: " + productCatagory.length());
			if(catagory.equals(productCatagory)) {
				System.out.println("Matched by "  + productInfo.getProductName());
				productCatagoryItemList.add(productInfo);
			}
		}
		
		//to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		modelAndView.addObject("productCatagoryItemList", productCatagoryItemList);
		modelAndView.addObject("catagoryName", productCatagory);
				
		return modelAndView;
	}

}
