package com.ecommerce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;

@Controller
@RequestMapping(value="/")
public class MainPageController {
	
	@RequestMapping(value="/")
	public ModelAndView mainPage(HttpServletRequest request) throws IOException {
		
		ModelAndView model;
		
		//checking if the user is logged in or not
		try {
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				System.out.println("User Logged in");
				System.out.println("Name: " + request.getSession().getAttribute("userName"));
				model = new ModelAndView("/User/MainPage");
			} else {
				System.out.println("No name. Logged Out");
				model = new ModelAndView("/Guest/MainPage");
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			model = new ModelAndView("/Guest/MainPage");
		}
		
		
		//retrive all the product information here
		List<ProductInformation> productDatabase = null;	//initially nothing stored
		ArrayList<ProductInformation> productFilteredList = new ArrayList<>();	//if there is a product out of stock, then we skip it
		
		
		//fetching database to gett all the product information
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();	//got all the product information
			System.out.println("Size of product Information: " + productDatabase.size());
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return model;
		}	
		
		
		
		/*
		 * here got all the product and filter there category
		 * also need to check product store
		 * if out of store then there is a functionability to remove that from the product list
		*/
		for(ProductInformation productInfo : productDatabase) {
			String catagory = productInfo.getProductCatagory();
			
			//if product is available then we got it
			if(productInfo.getStock() >=0) {
				productFilteredList.add(productInfo);
			}
			
			System.out.println("Product Name: " + productInfo.getProductName());
			System.out.println("Product Stock: " + productInfo.getStock());
			
		}
		
		
		
		
		//sorting the product
		for(int index=0; index<productDatabase.size(); index++) {
			ProductInformation pi1 = productDatabase.get(index);
			for(int index2=0; index2<productDatabase.size(); index2++) {
				ProductInformation pi2 = productDatabase.get(index2);
				if(pi1.getHit()>pi2.getHit()) {
					Collections.swap(productDatabase, index, index2);
				}
			}
		}
		
		
		//to display the top viewer product
		ArrayList<ProductInformation> alProductsDisplay = new ArrayList<>();	//no of top viewer product in the homepage
		ArrayList<ProductInformation> alNavDisplay = new ArrayList<>();		//select the 3 product to the nav bar
		
		//may not have 3 product overall
		try {
			for(int index=0; index<productDatabase.size(); index++) {
				alProductsDisplay.add(productDatabase.get(index));
				if(index<3) {
					alNavDisplay.add(productDatabase.get(index));
				}
			}
		} catch(Exception e) {
			System.out.println("Exception: " + e.toString());
		}
		
		/////////////////////////////--------------------checking pc
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		model.addObject("productCatagoryList", productCatagoryList);
		////////////////////////----------------------------------
		
		try {
			//model.addObject("productDatabaseList", productDatabase);
			model.addObject("productDatabaseList", alProductsDisplay);
			model.addObject("navProductList", alNavDisplay);
			
			model.addObject("firstProduct", alProductsDisplay.get(0));
			model.addObject("secondProduct", alProductsDisplay.get(1));
			model.addObject("thirdProduct", alProductsDisplay.get(2));
		} catch(Exception e) {
			System.out.println("Exception " + e.toString());
		}
		
		return model;
	}
	
}
