package com.ecommerce.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.ProductReviews;
import com.ecommerce.model.UserInfo;
import com.ecommerce.model.UserWishList;

@Controller
@RequestMapping("/product")
public class ProductPageController extends HttpServlet{
	
	@RequestMapping("/productPage.html")
	public ModelAndView AddingProduct(HttpServletRequest request) {
		System.out.println("Product Name: " + request.getParameter("productName"));
		System.out.println("This is a product page");
		ModelAndView model;
		
		try {
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				System.out.println("User Logged in");
				System.out.println("Name: " + request.getSession().getAttribute("userName"));
				model = new ModelAndView("/User/ProductPage");
			} else {
				System.out.println("No name. Logged Out");
				model = new ModelAndView("Guest/ProductPage");
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			model = new ModelAndView("Guest/MainPage");
		}
		
		List<ProductInformation> productDatabase = null;
		List <ProductReviews> reviewsDatabase = null;
		List<UserWishList> userWishListDatabase = null;		//all wishList
		List<UserInfo> userDatabase = null;		//all userInfo
		
		ArrayList<Integer> alProductIdList = new ArrayList<>();
		ArrayList<ProductInformation> alProductInformationList = new ArrayList<>();	//it will be passed to the client side
		
		ProductInformation productInformation = new ProductInformation();
		ProductReviews productReviews = new ProductReviews();
		//find the product to display information
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			reviewsDatabase = session.createQuery("from ProductReviews").list();
			userWishListDatabase = session.createQuery("from UserWishList").list();
			userDatabase = session.createQuery("from UserInfo").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return model;
		}	
		
		String name = request.getParameter("productName");
		int id;
		
		for(ProductInformation productInfo : productDatabase) {
			String productName = productInfo.getProductName();
			if(name.equals(productName)) {
				productInformation = productInfo;
				break;
			}
		}
		
		//define for the product availabelity
		if(productInformation.getStock() <= 0) {
			model.addObject("productAvailability", "Not Available");
		} else {
			model.addObject("productAvailability", "");
		}
		
		System.out.println("Size of reviews: " + reviewsDatabase.size());
		id = productInformation.getProductId();
		
		System.out.println("Current Product Name: " + productInformation.getProductName());
		System.out.println("Current Product Id: " + productInformation.getProductId());
		
		ArrayList<ProductReviews> currentProductReviews = new ArrayList<>();
		
		System.out.println("///////////////////All reviews");
		for(ProductReviews pr : reviewsDatabase) {
			System.out.println("id: " + id + " pr.id: " + pr.getProductId());
			System.out.println(" pr review: " + pr.getProductReview());
			if(pr.getProductId() == id) {
				System.out.println("Adding the product revews");
				currentProductReviews.add(pr);
			}
		}
		
		System.out.println("Sze of current product reviews is: " + currentProductReviews.size());
		
		//print the current prioduct reviews
		System.out.println("///////////////////Review of current product");
		for(ProductReviews pr : currentProductReviews) {
			System.out.println("Product Id: " + pr.getProductId());
			System.out.println("Product Review: " + pr.getProductReview());
			System.out.println("User Id: " + pr.getUserId());
			System.out.println("User Name: " + pr.getUserName());
		}
		
		byte[] productImage = productInformation.getProductImage();
		BufferedImage img = null;
		try {
			img = ImageIO.read(new ByteArrayInputStream(productImage));
		} catch (Exception e) {
			System.out.println("Exception: " + e.toString());
		}
		
		//update product info with a one more hit on viewing this page
		try {
			System.out.println("Updating");
			productInformation.setHit(productInformation.getHit() + 1);
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(productInformation);
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return model;
		}
		
		//here we get all the wishlist of the user
		String userName;	//this is user name
		int userId;	//this is the current user name id
		userName = request.getSession().getAttribute("userName").toString();	//now we know the current user name
		
		UserInfo userInfo = new UserInfo();		//current user info goes here
		
		//find the user info, matched with this name
		for(UserInfo uInfo : userDatabase) {	//traverse all the user info
			String uName = uInfo.getUserName();		//got each user info name
			if(userName.equals(uName)) {	//found the user info	
				userInfo = uInfo;		//store the current user info
				break;	//got mc object, no need to traverse anymore
			}
		}
		
		userId = userInfo.getUserId();	//store the user id of the current user using the user info
		
		model.addObject("cartExistence", "false");	//initially we assume, product is not in the cart
		
		//all the wished product id will be stored in the arraylist
		for(UserWishList uwl : userWishListDatabase) {		//traverse the user wishlist
			if(uwl.getUserId() == userId) {		//in wish list we got the user id and according to it we will get the product id 
				if(productInformation.getProductId() == uwl.getProductId()) {
					System.out.println("-----------------product is already added to the cart");
					model.addObject("cartExistence", "true");
				}
			}
		}
		
		///to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		model.addObject("productCatagoryList", productCatagoryList);
		
		model.addObject("productInformation", productInformation);
		model.addObject("productReviews", currentProductReviews);
		model.addObject("img", img);
		return model;
	}
	
	@RequestMapping("/newWishList.html")
	public ModelAndView addToWishList2(HttpServletRequest request) {
		//after added to the wishlist we will redirect to the cart viewer
		ModelAndView modelAndView = new ModelAndView("redirect:/myCartController/showMyCart.html");		
		
		//grab the product id, that have to add to the product page
		int productId = Integer.valueOf(request.getParameter("productId"));
		System.out.println("Adding a new Wishlist portion.");
		System.out.println("Name: " + " un");
		System.out.println("Id: " + productId);
		
		
		/*
		 * This is a temporary procedure
		 * Here we get the user name and 
		 * to get the user id first we get the
		 * all user info from the database and then we find
		 * the user id
		*/
		List<UserInfo> userDatabase = null;	//all user info
		UserInfo userInfo = new UserInfo();	//current user info
		//to update the product stock we need the product information, we get it from database
		List<ProductInformation> productInformationDatabase = null;
		ProductInformation productInformation = new ProductInformation(); //mc objcet we need to update
		
		//get the user info to find the logged in user and then update its wishliet
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			userDatabase = session.createQuery("from UserInfo").list();	//got all the user info from database 
			productInformationDatabase = session.createQuery("from ProductInformation").list();	//got all the product information
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			
		}
		
		//retrive current user name
		String userName;
		userName = request.getSession().getAttribute("userName").toString();
		
		//useing the current user name, we will grab the curretn user info
		for(UserInfo uInfo : userDatabase) {
			String uName = uInfo.getUserName();
			if(userName.equals(uName)) {
				userInfo = uInfo;
				break;
			}
		}
				
		System.out.println("User Id: " + userInfo.getUserId());
		
		//now we the user id and we will update the wish list using this
		int userId = userInfo.getUserId();
		
		//create user wishlist object using the userid and product id
		UserWishList userWishList = new UserWishList();
		userWishList.setProductId(productId);
		userWishList.setUserId(userId);
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(userWishList);	//inserting the data
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		/*
		 * Now a product is added to the 
		 * cart, so we need to decrease the
		 * product stock
		 * POCEDURE
		 * get the product id
		 * find the productInformation using the product id
		 * then update the product information by decreasing one stock
		*/
		//here we find the product
		for(ProductInformation pi : productInformationDatabase) {
			if(pi.getProductId() == productId) {
				productInformation = pi;	//got the product
				break;
			}
		}
		
		//decrease the stock
		productInformation.setStock(productInformation.getStock() - 1);
		
		//now we update the product
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(productInformation);	//update the product information
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		return modelAndView;
		
	} 
	
	@RequestMapping("/addReview")
	private ModelAndView addProductReview(HttpServletRequest request) {
		ModelAndView modelAndView;
		String productName = "";		
		System.out.println("This is adding product review section.");
		
		
		
		int productId = Integer.valueOf(request.getParameter("productId"));
		String productReview = request.getParameter("productReview");
		int userId = -1;
		String userName = "Anonymous";
		
		System.out.println("product Id: " + productId);
		System.out.println("product Review: " + productReview);
		
		
		//checkign if the user is logged in
		//if logged in then we will grub the username and the user id
		try {
			
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				userName = (String) request.getSession().getAttribute("userName");
				userId = (int) request.getSession().getAttribute("userId");
			} else {
				System.out.println("No name. Logged Out");
				
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			System.out.println("may be a problm occcoured");
		}
		
		ProductReviews productReviews = new ProductReviews();
		productReviews.setProductId(productId);
		productReviews.setProductReview(productReview);
		productReviews.setUserId(userId);
		productReviews.setUserName(userName);
		
		List<ProductInformation> productInformationList = null;
		
		try {
			
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(productReviews);
			productInformationList = session.createQuery("from ProductInformation").list();
			session.getTransaction().commit();
			session.close();
			
		} catch(Exception e) {
			System.out.println("Exception: on store comment: " + e.toString());
		}
		
		ProductInformation productInformation = new ProductInformation();
		
		for(ProductInformation pi : productInformationList) {
			if(pi.getProductId() == productId) {
				productName = pi.getProductName();
				break;
			}
		}
		
		modelAndView = new ModelAndView("redirect:/product/productPage.html?productName=" + productName);
		
		return modelAndView;
	}
	
	
	
}
