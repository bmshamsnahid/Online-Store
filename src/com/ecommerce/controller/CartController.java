package com.ecommerce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.json.JSONArray;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.Administrator;
import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;
import com.ecommerce.model.UserInfo;
import com.ecommerce.model.UserWishList;

@Controller
@RequestMapping(value="/myCartController")
public class CartController {
	
	/*
	 * First we get all the product information, user information and wish list
	 * Then filter the current user wishlistt
	 * finally from the current product wishlist we put the product information to the arraylist
	 * arrayList will be passed to the client side
	*/
	@RequestMapping(value="/showMyCart.html")
	public ModelAndView addAdmin(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/User/Cart");
		System.out.println("This is my cart controller");
		
		int totalMoney = 0;
		
		List<UserInfo> userDatabase = null;		//all userInfo
		List<UserWishList> userWishListDatabase = null;		//all wishList
		List<ProductInformation> productDatabase = null;	//all product Information
		
		ArrayList<Integer> alProductIdList = new ArrayList<>();
		ArrayList<ProductInformation> alProductInformationList = new ArrayList<>();	//it will be passed to the client side
		
		
		UserInfo userInfo = new UserInfo();
		
		//get the user info to find the logged in user and then update its wishliet
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all userInfo, userWishList and product information from productInformation table
			userDatabase = session.createQuery("from UserInfo").list();
			userWishListDatabase = session.createQuery("from UserWishList").list();
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return modelAndView;
		}
		
		String userName;
		int userId;
		userName = request.getSession().getAttribute("userName").toString();	//now we know the current user name
		
		//find the user info, matched with this name
		for(UserInfo uInfo : userDatabase) {
			String uName = uInfo.getUserName();
			if(userName.equals(uName)) {	//found the user info
				userInfo = uInfo;	
				break;
			}
		}
		
		userId = userInfo.getUserId();	//store the user id of the current user using the user info
		
		//all the wished product id will be stored in the arraylist
		for(UserWishList uwl : userWishListDatabase) {
			if(uwl.getUserId() == userId) {		//in wish list we got the user id and according to it we will get the product id 
				alProductIdList.add(uwl.getProductId());	//added to the product id
			}
		}
		
		//Now we will traverse the whole product information and add all the user wished product info
		for(ProductInformation pi : productDatabase) {
			if(alProductIdList.contains(pi.getProductId())) {	//got product, that matched with user wish list
				alProductInformationList.add(pi);	//add it to pass to the client side
				totalMoney += Integer.parseInt(pi.getProductPrice());
			}
		}
		
		//to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		modelAndView.addObject("alProductInformationList", alProductInformationList);
		modelAndView.addObject("size", alProductInformationList.size());
		modelAndView.addObject("name", "Nahid");
		modelAndView.addObject("totalMoney", totalMoney);
		
		return modelAndView;
	}
	
	/*
	 * To remove a product
	 * Grab All the user wish lsit database
	 * Then detect the current product that will be deleted by matching product id and user id
	 * Then we remove the detected product from user wish list
	*/
	@RequestMapping(value="/removeProduct.html")
	public ModelAndView removeFromCart(HttpServletRequest request) {
		System.out.println("Removing Product");
		
		int productId = Integer.parseInt(request.getParameter("productId"));
		Integer userId = (Integer) request.getSession().getAttribute("userId");
		
		
		List<UserWishList> userWishListDatabase = null;		//all wishList
		UserWishList userWishList = new UserWishList();
		//grab all the user wish list database
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			userWishListDatabase = session.createQuery("from UserWishList").list();
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//find the mc product that must be removed
		for(UserWishList uwl : userWishListDatabase) {
			if(uwl.getUserId() == userId && uwl.getProductId() == productId) {		//in wish list we got the user id and according to it we will get the product id 
				userWishList = uwl;	//got it
				break;
			}
		}
		
		System.out.println("The desired product Information");
		System.out.println("Product Id: " + productId);
		System.out.println("User Id: " + request.getSession().getAttribute("userId"));
		
		System.out.println("Information of the product: ");
		System.out.println("Product Id: " + userWishList.getProductId());
		System.out.println("User id: " + userWishList.getUserId());
		
		
		//since a product is removed from the cart, the product stock is increased
		//here is the storage
		List<ProductInformation> productInformationDatabase = null;	//here we keep all the products
		
		//now remove the mc product 
		try {	
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(userWishList);	//remove the userlist object
			productInformationDatabase = session.createQuery("from ProductInformation").list();	// got all the product information
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
		}
		
		//we keep the mc product here
		ProductInformation productInformation = new ProductInformation();
		//Now we detect the product Information using the product id
		for(ProductInformation pi : productInformationDatabase) {
			if(pi.getProductId() == productId) {
				productInformation = pi;	//got the product inormation
				break;
			}
		}
		
		//update the productt stock by one plus
		productInformation.setStock(productInformation.getStock() + 1);
		
		//now update the database
		try {	
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(productInformation);	//remove the userlist object
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
		}
		
		ModelAndView model = new ModelAndView("redirect:/myCartController/showMyCart.html");
		return model;
	}
	
	/*
	 * Here we update the product stock
	 * Receive the product id
	 * retrive all the product information from the database
	 * find the product information
	 * then update the product stock
	 * update the product information database
	 * if everything ok then return true
	 * else return false
	*/
	private boolean increaseProductStock(int productId) {
		
		ProductInformation productInformation = new ProductInformation();
		List<ProductInformation> productInformationDatabase = null;
		
		//now got all the product information 
		try {	
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			productInformationDatabase = session.createQuery("from ProductInformation").list();	// got all the product information
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
		}
		
		//Now we detect the product Information using the product id
		for(ProductInformation pi : productInformationDatabase) {
			if(pi.getProductId() == productId) {
				productInformation = pi;	//got the product inormation
				break;
			}
		}
		
		//increase the product stock
		productInformation.setStock(productInformation.getStock() + 1);
		
		//now update the database
		try {	
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(productInformation);	//remove the userlist object
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
		}
		
		return true;
	}
	
	/*
	 * To Buy a all product
	 * Grab All the user wish lsit database
	 * Then detect the current product that will be deleted by matching product id and user id
	 * Then we remove the detected product from user wish list
	*/
	@RequestMapping(value="/buyProduct.html")
	public ModelAndView buyFromCart(HttpServletRequest request) {
		System.out.println("Removing Product");
		
		int productId = Integer.parseInt(request.getParameter("productId"));
		Integer userId = (Integer) request.getSession().getAttribute("userId");
		
		
		List<UserWishList> userWishListDatabase = null;		//all wishList
		UserWishList userWishList = new UserWishList();
		//grab all the user wish list database
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			userWishListDatabase = session.createQuery("from UserWishList").list();
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//find the mc product that must be removed
		for(UserWishList uwl : userWishListDatabase) {
			if(uwl.getUserId() == userId && uwl.getProductId() == productId) {		//in wish list we got the user id and according to it we will get the product id 
				userWishList = uwl;	//got it
				break;
			}
		}
		
		System.out.println("The desired product Information");
		System.out.println("Product Id: " + productId);
		System.out.println("User Id: " + request.getSession().getAttribute("userId"));
		
		System.out.println("Information of the product: ");
		System.out.println("Product Id: " + userWishList.getProductId());
		System.out.println("User id: " + userWishList.getUserId());
		
		
		//now remove the mc product 
		try {	
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(userWishList);
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
		}
		
		ModelAndView model = new ModelAndView("redirect:/myCartController/showMyCart.html");
		return model;
	}
	
	/*
	 * To remove all the carts product 
	 * we grab all the user catrs product and
	 * one by one remove all
	*/
	@RequestMapping("/removeAll.html")
	public ModelAndView removeAllProduct(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("redirect:/myCartController/showMyCart.html");
		
		System.out.println("Removing All Products");
		
		Integer userId = (Integer) request.getSession().getAttribute("userId");
		
		
		List<UserWishList> userWishListDatabase = null;		//all wishList
		UserWishList userWishList = new UserWishList();
		//grab all the user wish list database
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			userWishListDatabase = session.createQuery("from UserWishList").list();
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//find the mc product that must be removed
		for(UserWishList uwl : userWishListDatabase) {
			if(uwl.getUserId() == userId) {		//in wish list we got the user id and according to it we will get the product id 
				userWishList = uwl;	//got it
				increaseProductStock(userWishList.getProductId());
				try {	
					SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
					org.hibernate.Session session = sessionFactory.openSession();
					session.beginTransaction();
					session.delete(userWishList);
					session.getTransaction().commit();
					session.close();
				} catch(Exception e) {
					System.out.println("Exception on storing in database: " + e.toString());
				}
			}
		}
		
		return modelAndView;
	}
	

}
