package com.ecommerce.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;
import com.ecommerce.model.UserInfo;

@Controller
@RequestMapping(value="/loginController")
public class LogInController {
	
	List<UserInfo> userDatabase = null;
	boolean authenticationChecker = false;
	boolean wrongPassword = false;
	boolean notAnUser = true;
	
	@RequestMapping(value="/loginForm.html")
	public ModelAndView getLogInForm() {
		ModelAndView model = new ModelAndView("LogInForm");
		return model;
	}
	
	@RequestMapping(value="/loginSuccess.html")
	public ModelAndView loginSuccess(@ModelAttribute("user") User userInput, HttpServletRequest request) {
		System.out.println("Log in success Apperared");
		request.getSession().setAttribute("LoggedIn", "false");
		UserInfo desiredUserInfo = new UserInfo();
		
		authenticationChecker = false;
		 
		try {	
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			//retrive userName and userPassword fron user table
			userDatabase = session.createQuery("from UserInfo").list();
			
			session.beginTransaction();
			session.close();
			
			//System.out.println("Input name: " + userInput.getUserName() + " Password: " + userInput.getUserPassword());
			
			//Compare Users Input userName and userPassword with user Database 
			for(UserInfo userTemp : userDatabase) {
				//System.out.println("name: " + userTemp.getUserName() + " and Password: " + userTemp.getUserPassword());
				if(userInput.getUserName().equals(userTemp.getUserName())) {
					
					if(userTemp.getUserPassword().equals(userInput.getUserPassword())) {
						System.out.println("Input Name");
						authenticationChecker = true;
						wrongPassword = false;
						notAnUser = false;
						desiredUserInfo = userTemp;
						break;
					} else {
						wrongPassword = true;
						notAnUser = false;
					}
					
				}
			}
			
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		if(authenticationChecker) {
			System.out.println("Authentication Successfull");
			
			request.getSession().setAttribute("loggedIn", "true");
			//request.getSession().setAttribute("userName", userInput.getUserName());
			request.getSession().setAttribute("userName", desiredUserInfo.getUserName());
			request.getSession().setAttribute("userId", desiredUserInfo.getUserId());
			
			
			return new ModelAndView("redirect:/");
		} else if(wrongPassword) {
			System.out.println("Authentication Failure: " + " Wrong userPassword.");
		} else if(notAnUser) {
			System.out.println("Authentication Failure: " + " userName does not Exist.");
		} else {
			System.out.println("Authentivation Failure: " + " Unknown Error Ocours.");
		}
		System.out.println("Authintication Failed");
		return new ModelAndView("/Generic/Registration");
	}
	
	@RequestMapping("/logOut")
	public ModelAndView logOut(HttpServletRequest request) {
		
		System.out.println("Logging Out");
		
		ModelAndView modelAndView = new ModelAndView("redirect:/");
		
		request.getSession().setAttribute("loggedIn", "false");
		
		return modelAndView;
	}
		
}

