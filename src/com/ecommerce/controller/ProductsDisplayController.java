package com.ecommerce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale.Category;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.json.JSONArray;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;

@Controller
@RequestMapping("/productsDisplay")
public class ProductsDisplayController {

	@RequestMapping("/display.html")
	public ModelAndView showProducts(HttpServletRequest request) {
		
		ModelAndView modelAndView;
		
		try {
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				System.out.println("User Logged in");
				System.out.println("Name: " + request.getSession().getAttribute("userName"));
				modelAndView = new ModelAndView("/User/ProductsDisplay");
			} else {
				System.out.println("No name. Logged Out");
				modelAndView = new ModelAndView("/Guest/ProductsDisplay");
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			modelAndView = new ModelAndView("Guest/MainPage");
		}
		
		List<ProductInformation> productDatabase = null;
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return modelAndView;
		}
		
		
		///to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		modelAndView.addObject("productDatabaseList", productDatabase);
		
		return modelAndView;
	}
	
	@RequestMapping("/categories.html")
	public ModelAndView showProductsByCategory(HttpServletRequest request) {
		
		ModelAndView modelAndView;
		
		try {
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				System.out.println("User Logged in");
				System.out.println("Name: " + request.getSession().getAttribute("userName"));
				modelAndView = new ModelAndView("/User/ProductsDisplayCategoies");
			} else {
				System.out.println("No name. Logged Out");
				modelAndView = new ModelAndView("/Guest/ProductsDisplayCategoies");
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			modelAndView = new ModelAndView("Guest/MainPage");
		}
		
		List<ProductInformation> productDatabase = null;
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return modelAndView;
		}
		
		
		//here we will separate all the products in category and each category contains at most 3 products
		
		///to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		HashMap<String, ArrayList<ProductInformation>> productCategoryMap = new HashMap<>();
		//for each category contains a arraylist of products conatains 3 products
		
		for(int index=0; index<productCatagoryList.size(); index++) {
			int i = 0;
			ArrayList<ProductInformation> piList = new ArrayList<>();
			
			String pc = productCatagoryList.get(index);
			System.out.println("Got product category: " + pc + " and i = " + i);
			
			for(int index2=0; index2<productDatabase.size(); index2++) {
				ProductInformation productInfo = productDatabase.get(index2);
				
				String catagory = productInfo.getProductCatagory();
				if(catagory.equals(pc)) {
					i += 1;
					piList.add(productInfo);
					System.out.println("From " + productInfo.getProductCatagory() + " category " + productInfo.getProductName() + " and i= " + i);
				}
				if(i >= 3) {
					i = 0;
					break;
				}
			}
			productCategoryMap.put(pc, piList);
			modelAndView.addObject(pc, piList);
		}
		
		
		
		modelAndView.addObject("productCategory", productCategoryMap);
		modelAndView.addObject("productDatabaseList", productDatabase);
		
		return modelAndView;
	}
	
}
