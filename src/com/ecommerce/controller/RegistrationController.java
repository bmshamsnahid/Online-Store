package com.ecommerce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.User;
import com.ecommerce.model.UserInfo;

@Controller
@RequestMapping(value="/registrationController")
public class RegistrationController {
	
	@RequestMapping(value="/registrationForm.html")
	public ModelAndView getRegistrationForm() {
		/*ModelAndView model = new ModelAndView("RegistrationForm");*/
		ModelAndView model = new ModelAndView("/Generic/Registration");
		
		//to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		model.addObject("productCatagoryList", productCatagoryList);
		
		return model;
	}
	
	@RequestMapping(value="/registrationComplete.html")
	public ModelAndView mainPage(HttpServletRequest request) throws IOException {
		
		UserInfo userInfo = new UserInfo();	//this is current new user, will be saved in the database
		User user = new User();	//not available in our recent version
		
		List<UserInfo> userDatabase = null;	// all user info goes here
		
		//fetch database to get all the user info
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			userDatabase = session.createQuery("from UserInfo").list();	//get all the usre info
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		/*
		 *1. user name and full name can not be same
		 *2. email must be *@*.com format(.com can be considered)
		 *3. Date of birth be year-month-day formate, and age must not be more than 115 and not less than 7
		 *4. sending mail
		 *5. generate random 4 diigt value
		 *6. Mobile number cheking for at least 10 country
		 *7 password and confirm password must be same
		*/
		
		//store the name, passwords, mail and cell number
		String userName = request.getParameter("userName");
		String fullName =  request.getParameter("userFullName");
		String userEmail = request.getParameter("userEmail");
		String userMobileNumber = request.getParameter("userMobileNumber");
		String userPassword = request.getParameter("userPassword");
		String userConformPassword = request.getParameter("userConfirmPassword");
		
		boolean errorFlag = false;	//let's assume no error exist
		String errorLog = "";
		
		//none of the field can be empty
		if(userName.length() <= 0 || fullName.length() <= 0 || userMobileNumber.length() <= 0 || userPassword.length() <= 0 || userConformPassword.length() <= 0 || userEmail.length() <= 0) {
			errorLog += "<br/>* Fill all input field";
			errorFlag = true;
		}
		//checking the email address
		if(!userEmail.contains("@") || !userEmail.contains(".com")) {
			errorLog += "<br/>* Invalid email address";
			errorFlag = true;
		}
		//checking password
		/*
		 * Password must be at least 6 character
		 * matched with confirm password
		*/
		//checking password length
		if(userPassword.length() < 6) {
			errorLog += "<br/>* Password must be at least 6 characters.";
			errorFlag = true;
		}
		//matching password
		if(!userPassword.equals(userConformPassword)) {
			errorLog += "<br/>* Password does not match";
			errorFlag = true;	
		}
		
		//checking if already a email or username is taken
		for(UserInfo ui : userDatabase) {
			try {
				if(ui.getUserEmail().equals(userEmail)) {
					errorLog += "<br/>* Email Exist, Recover account.";
					errorFlag = true;
				} 
				if(ui.getUserName().equals(userName)) {
					errorLog += "<br/>* User name already taken.";
					errorFlag = true;
				}
			} catch(Exception e) {
				System.out.println("Exception on database value: encounter null values");
			}
		}
		
		System.out.println("------------------------Error Log: \n"+ errorLog);
		
		if(errorFlag == true) {
			ModelAndView model = new ModelAndView("/Generic/Registration");
			model.addObject("errorLog", errorLog);
			return model;
		} else {
			user.setUserName(request.getParameter("userName"));
			user.setUserPassword(request.getParameter("userPassword"));
			
			
			userInfo.setUserEmail(request.getParameter("userEmail"));
			userInfo.setUserFullName(request.getParameter("userFullName"));
			userInfo.setUserName(request.getParameter("userName"));
			//userInfo.setUserDateOfBirth(request.getParameter("userDateOfBirth"));
			//userInfo.setUserSex(request.getParameter("userSex"));
			userInfo.setUserMobileNumber(request.getParameter("userMobileNumber"));
			//userInfo.setUserPreferredContactMethod(request.getParameter("userPreferredContactMethod"));
			//userInfo.setUserCountry(request.getParameter("userCountry"));
			userInfo.setUserPassword(request.getParameter("userPassword"));
			
			
			try {
				SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
				org.hibernate.Session session = sessionFactory.openSession();
				session.beginTransaction();
				session.save(userInfo);
				session.save(user);
				session.getTransaction().commit();
				session.close();
			} catch(Exception e) {
				System.out.println("Exception on storing in database: " + e.toString());
				return new ModelAndView("RegistrationForm");
			}
			
			ModelAndView model = new ModelAndView("/Generic/LogInForm");
			model.addObject("errorLog", "");
			
			return model;
		}
	}

}
