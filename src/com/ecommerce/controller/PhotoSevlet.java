package com.ecommerce.controller;

import java.io.IOException;
import java.sql.DriverManager;

import javax.resource.cci.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class PhotoSevlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {

	    final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	    final String DB_URL = "jdbc:mysql://localhost:3306/E-CommerceDemo";
	    final String User = "root";
	    final String Password = "56316498";
	    try {
	        Class.forName(JDBC_DRIVER);
	        Connection conn = (Connection) DriverManager.getConnection(DB_URL, User, Password);

	        PreparedStatement stmt = (PreparedStatement) conn.prepareStatement("select photo from users where id=?");
	        stmt.setLong(1, Long.valueOf(request.getParameter("id")));
	        ResultSet rs = (ResultSet) stmt.executeQuery();
	        if (rs.next()) {
	            response.getOutputStream().write(rs.getBytes("photo"));
	        }
	        conn.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

}
