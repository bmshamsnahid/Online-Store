package com.ecommerce.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;

@Controller
@RequestMapping(value="/genericController")
public class GenericController {
	
	@RequestMapping("/search")
	public ModelAndView searchProduct(HttpServletRequest request) {
		ModelAndView modelAndView;
		
		System.out.println("Searching");
		
		try {
			if(request.getSession().getAttribute("loggedIn").equals("true")) {
				System.out.println("User Logged in");
				System.out.println("Name: " + request.getSession().getAttribute("userName"));
				modelAndView = new ModelAndView("/User/SearchProduct");
			} else {
				System.out.println("No name. Logged Out");
				modelAndView = new ModelAndView("/Guest/SearchProduct");
			}
		} catch(Exception e) {
			System.out.println("No name. Logged Out");
			modelAndView = new ModelAndView("Guest/MainPage");
		}
		
		
		List<ProductInformation> productDatabase = null;
		ArrayList<ProductInformation> productList = new ArrayList<>();
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return new ModelAndView("MainPage");
		}	
		
		String searchKeyword = request.getParameter("searchKeyword").toLowerCase();
		System.out.println("Searching: " + searchKeyword);
		
		
		for(ProductInformation productInfo : productDatabase) {
			String name = productInfo.getProductName().toLowerCase();
			/*if(name.equals(searchKeyword)) {
				productList.add(productInfo);
			}*/
			if(name.contains(searchKeyword)) {
				productList.add(productInfo);
			}
		}
		
		///to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		modelAndView.addObject("productList", productList);
		
		return modelAndView;
	}
	
	/*
	 * Here we first get all the products
	 * Then from the products category we get all the category list
	 * add them to the array lsit
	 * return the category list
	*/
	public ArrayList<String> getProductsCategory() {
		ArrayList<String> productsCategory = new ArrayList<>();
		
		//from database we retrive all the product information and keep them to this list
		List<ProductInformation> productInformationDatabase = null;	
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productInformationDatabase = session.createQuery("from ProductInformation").list();	//got all the product information
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//now all the product information we retrive the category
		//if the category is not inserted to the arraylist
		//then we add the category to the arraylist
		for(ProductInformation pi : productInformationDatabase) {
			String cat = pi.getProductCatagory();
			if(productsCategory.contains(cat) == false) {
				productsCategory.add(cat);
			}
		}
		
		return productsCategory;
	}

}
