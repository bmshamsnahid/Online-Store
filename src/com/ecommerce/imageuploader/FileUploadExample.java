package com.ecommerce.imageuploader;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.staticElements.StaticElement;

@Controller
public class FileUploadExample extends HttpServlet {
	public static String productName;
	
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		 System.out.println(this.productName);
		 System.out.println("This is a file upload example servlet");
		
		 ServletContext context = null;
		 
		 boolean isMultipart = ServletFileUpload.isMultipartContent(request);
	        
	        
	        if (isMultipart) {
	        	// Create a factory for disk-based file items
	        	FileItemFactory factory = new DiskFileItemFactory();

	        	// Create a new file upload handler
	        	ServletFileUpload upload = new ServletFileUpload(factory);
	 
	            try {
	            	// Parse the request
	            	List /* FileItem */ items = upload.parseRequest(request);
	                Iterator iterator = items.iterator();
	                while (iterator.hasNext()) {
	                    FileItem item = (FileItem) iterator.next();
	                    if (!item.isFormField()) {
	                        String fileName = item.getName();	 
	                        String root = getServletContext().getRealPath("/");
	                        //File path = new File("/home/bmshamsnahid/Tomcat Server/apache-tomcat-7.0.69/webapps/ROOT/WEB-INF/uploads");
	                        //File path = new File("/resources");
	                        File path = new File("/home/bmshamsnahid/ECommerceDemoImages");
	                        if (!path.exists()) {
	                            boolean status = path.mkdirs();
	                        }
	                        
	                        
	                        //-------------------                
	                        //File uploadedFile = new File(path + "/" + fileName);
	                        File uploadedFile = new File(path + "/" + this.productName + ".jpg");
	                        System.out.println(uploadedFile.getAbsolutePath());
	                        item.write(uploadedFile);
	                    }
	                }
	            } catch (FileUploadException e) {
	                e.printStackTrace();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	        response.sendRedirect("/E-CommerceDemo/addProductController/addProduct.html");
	       // return new ModelAndView("redirect:/upload?productName=");
	        
	    }
	 
	 @RequestMapping(value="/upload")
	 public ModelAndView uploadFile(HttpServletRequest request) {
		 String productName = request.getParameter("productName");
		 this.productName = productName; 
         //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>> Product Name: " + this.productName);
		 ModelAndView modelAndView = new ModelAndView("/Administrator_/FileUpload");
		 modelAndView.addObject("productName", productName);
		 new StaticElement().productName = productName;
		 return modelAndView;
	 }
	 
	 @RequestMapping(value="/display")
	 public ModelAndView displayFile() {
		 return new ModelAndView("displayFile");
	 }

}