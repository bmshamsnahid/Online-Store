package com.ecommerce.model;

import javax.persistence.*;

@Entity
@Table(name="PRODUCT_INFORMATION")
public class ProductInformation {
	@Id
	@GeneratedValue
	private int productId;
	private String productName;
	private String productPrice;
	private String productCatagory;
	private byte[] productImage;
	private String productShortInformation;
	private String productFullInformation;
	private int hit;
	private int reviews;
	private int stock;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public byte[] getProductImage() {
		return productImage;
	}
	public void setProductImage(byte[] productImage) {
		this.productImage = productImage;
	}
	public String getProductShortInformation() {
		return productShortInformation;
	}
	public void setProductShortInformation(String productShortInformation) {
		this.productShortInformation = productShortInformation;
	}
	public String getProductFullInformation() {
		return productFullInformation;
	}
	public void setProductFullInformation(String productFullInformation) {
		this.productFullInformation = productFullInformation;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductCatagory() {
		return productCatagory;
	}
	public void setProductCatagory(String productCatagory) {
		this.productCatagory = productCatagory;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getHit() {
		return hit;
	}
	public void setHit(int hit) {
		this.hit = hit;
	}
	public int getReviews() {
		return reviews;
	}
	public void setReviews(int reviews) {
		this.reviews = reviews;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
		
}
