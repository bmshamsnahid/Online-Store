package com.ecommerce.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="USER_INFO")
public class UserInfo {
	@Id
	@GeneratedValue
	private int userId;
	private String userEmail; 
	private String userFullName;
	private String userName;
	private String userDateOfBirth;
	private String userSex;
	private String userMobileNumber;
	private String userPreferredContactMethod;
	private String userCountry;
	private String userPassword;
	private String wishList;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserFullName() {
		return userFullName;
	}
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserDateOfBirth() {
		return userDateOfBirth;
	}
	public void setUserDateOfBirth(String userDateOfBirth) {
		this.userDateOfBirth = userDateOfBirth;
	}
	public String getUserSex() {
		return userSex;
	}
	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}
	public String getUserMobileNumber() {
		return userMobileNumber;
	}
	public void setUserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}
	public String getUserPreferredContactMethod() {
		return userPreferredContactMethod;
	}
	public void setUserPreferredContactMethod(String userPreferredContactMethod) {
		this.userPreferredContactMethod = userPreferredContactMethod;
	}
	public String getUserCountry() {
		return userCountry;
	}
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getWishList() {
		return wishList;
	}
	public void setWishList(String wishList) {
		this.wishList = wishList;
	}
}
