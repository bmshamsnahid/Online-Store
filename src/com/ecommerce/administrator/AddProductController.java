package com.ecommerce.administrator;

import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.*;
import com.ecommerce.model.ProductInformation;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import javassist.bytecode.annotation.BooleanMemberValue;

@Controller
@RequestMapping(value="/addProductController")
public class AddProductController extends HttpServlet {
	
	@RequestMapping("/addProduct.html")
	public ModelAndView AddingProduct() {
		ModelAndView model = new ModelAndView("/Administrator_/AddProduct");
		
		//passing the product category
		List<ProductInformation> productDatabase = null;
		ArrayList<String> productCatagoryList = new ArrayList<>();
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			System.out.println("Size of product Information: " + productDatabase.size());
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return model;
		}	
		
		
		for(ProductInformation productInfo : productDatabase) {
			String catagory = productInfo.getProductCatagory();
			if(!productCatagoryList.contains(catagory)) {
				productCatagoryList.add(catagory);
			}
		}
		
		model.addObject("productCatagoryList", productCatagoryList);
		
		return model;
	}
	
	@RequestMapping("/addingProductSuccess.html")
	public ModelAndView addProductSuccess(HttpServletRequest request)  {
		//ModelAndView model = new ModelAndView("AddingProductSuccess");
		ModelAndView model = new ModelAndView("redirect:/upload?productName=" + request.getParameter("productName"));
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Sending: " + request.getParameter("productName"));
		ProductInformation productInformation = new ProductInformation();
		
		productInformation.setProductName(request.getParameter("productName"));
		productInformation.setProductCatagory(request.getParameter("productCatagory"));
		productInformation.setProductPrice(request.getParameter("productPrice"));
		productInformation.setProductShortInformation(request.getParameter("productShortInformation"));
		productInformation.setProductFullInformation(request.getParameter("productFullInformation"));
		System.out.println("Stock: " + request.getParameter("stock"));
		//productInformation.setStock(Integer.parseInt(request.getParameter("stock")));
		productInformation.setStock(Integer.valueOf(request.getParameter("stock")));
		
		//Reading File
		File file = new File(request.getParameter("productImage"));
		byte[] bFile = new byte[(int)file.length()];
		try{
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();
		} catch(Exception e) {
			System.out.println("Exception in File Handling.");
		}
		productInformation.setProductImage(bFile);		
		
		//String file = request.getParameter("productImage");
		System.out.println("Product Name is: " + productInformation.getProductName());
		System.out.println("Product Price is: " + productInformation.getProductPrice());
		System.out.println("Product Catagory is: " + productInformation.getProductCatagory());
		System.out.println("Product Short Information is: " + productInformation.getProductShortInformation());
		System.out.println("Product Full Information is: " + productInformation.getProductFullInformation());
		System.out.println("Product Stock: " + productInformation.getStock());
		System.out.println("Image: " + file.toString());
		
		
		try {
			
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(productInformation);
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
			return new ModelAndView("AddProduct");
		}
		
		return model;
	}
	
}
