package com.ecommerce.administrator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.controller.GenericController;
import com.ecommerce.model.Administrator;
import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.UserWishList;

@Controller
@RequestMapping(value="/deleteOrModify")
public class DeleteOrModifyProductController extends HttpServlet {
	
	
	@RequestMapping(value="/show")
	public ModelAndView mainPage(HttpServletRequest request) throws IOException {
		
		ModelAndView model = new ModelAndView("/Administrator_/DeleteOrModifyProduct");
		
		List<ProductInformation> productDatabase = null;	//all the product information goes here
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory(); 
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();	//got all the product information
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}	
		
		
		
		
		model.addObject("productDatabaseList", productDatabase);	//sending to the client side
		return model;
	}
	
	@RequestMapping(value="/deleteProduct")
	public ModelAndView deleteProduct(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("redirect:/deleteOrModify/show");
		
		String productName = request.getParameter("productName");
		
		ProductInformation productInformation = new ProductInformation();
		
		
		List<ProductInformation> productDatabase = null;
		ArrayList<String> productCatagoryList = new ArrayList<>();
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}	
		
		for(ProductInformation productInfoTemp : productDatabase) {
			String productNameTemp = productInfoTemp.getProductName();
			if(productNameTemp.equals(productName)) {
				productInformation = productInfoTemp;
			}
		}
		
		System.out.println("Deleting " + productInformation.getProductName());
		
		
		/*
		 * When delete a product, user wishlist relate to the product will be removed
		 * first get the product id
		 * get all the product wishlist
		 * if there is a product relate to the wishlist, we delete it
		*/
		int productId = productInformation.getProductId();	//got the  product id
		List<UserWishList> userWishListDatabase = null;
		UserWishList userWishList = new UserWishList();
		
		//here we get all the userWishlist information and delete product
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			session.delete(productInformation);	//delete the product
			userWishListDatabase = session.createQuery("from UserWishList").list();		//get the user wishList
			
			session.getTransaction().commit();
			session.close();	
		} catch(Exception e) {
			System.out.println("Exception: " + e.toString());
		}
		
		//Now each wishlist matched with the current product id will be deleted
		for(UserWishList uwl : userWishListDatabase) {
			if(uwl.getProductId() == productId) {
				try {
					SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
					org.hibernate.Session session = sessionFactory.openSession();
					session.beginTransaction();	
					session.delete(uwl);	//delete the wishlist
					session.getTransaction().commit();
					session.close();	
				} catch(Exception e) {
					System.out.println("Exception: " + e.toString());
				}
			}
		}		
		
		return modelAndView;
	}
	
	@RequestMapping(value="/modifyProduct.html")
	public ModelAndView modifyProduct(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/Administrator_/ModifyProduct");
		ProductInformation productInformation = new ProductInformation();
		
		String productName = request.getParameter("productName");
		productInformation = getProductInformation(productName);
		
		System.out.println("Product name: " + productInformation.getProductFullInformation());
		
		modelAndView.addObject("productInformation", productInformation);
		
		//We don't delete it, just update it
		///delete(productInformation);
		
		//to display category in the header section
		ArrayList<String> productCatagoryList;
		productCatagoryList = new ArrayList<>();
		productCatagoryList = new GenericController().getProductsCategory();
		modelAndView.addObject("productCatagoryList", productCatagoryList);
		
		return modelAndView;
		
	}
	
	@RequestMapping(value="/modifyProductSuccess.html")
	public ModelAndView modifyProductSuccess(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("redirect:/deleteOrModify/show");
		
		ProductInformation productInformation = new ProductInformation();
		
		//got information of the product
		productInformation.setProductId(Integer.parseInt(request.getParameter("productId")));
		productInformation.setProductName(request.getParameter("productName"));
		productInformation.setProductCatagory(request.getParameter("productCatagory"));
		productInformation.setProductPrice(request.getParameter("productPrice"));
		productInformation.setProductShortInformation(request.getParameter("productShortInformation"));
		productInformation.setProductFullInformation(request.getParameter("productFullInformation"));
		productInformation.setStock(Integer.parseInt(request.getParameter("productStock")));
		
		/*	PROBABLY NOT NEEDED
		 * To get the product id
		 * we first discover the product in datavase
		 * then get the product matched by name
		 * then we retrive the product id
		*/		
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			//not being seva, we update the object
			//session.save(productInformation);
			session.update(productInformation);
			
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
		}
		
		return modelAndView;
	}
	
	private ProductInformation getProductInformation(String productName) {
		ProductInformation pi = new ProductInformation();
		
		List<ProductInformation> productDatabase = null;
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		for(ProductInformation productInfoTemp : productDatabase) {
			String productNameTemp = productInfoTemp.getProductName();
			if(productNameTemp.equals(productName)) {
				pi = productInfoTemp;
				break;
			}
		}
		
		return pi;
	}
	
	private void delete(ProductInformation pi) {
		try {
			
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(pi);
			session.getTransaction().commit();
			session.close();
			
		} catch(Exception e) {
			System.out.println("Exception: " + e.toString());
		}
	}
		
	
}
