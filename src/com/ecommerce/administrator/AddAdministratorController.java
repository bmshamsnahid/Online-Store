package com.ecommerce.administrator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.Administrator;

@Controller
@RequestMapping(value="/addAdministratorController")
public class AddAdministratorController extends HttpServlet {

	@RequestMapping(value="/addAdministrator.html")
	public ModelAndView addAdmin(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/Administrator_/AddAdmin");
		
		
		
		return modelAndView;
	}
	
	@RequestMapping(value="/addAdminSuccess.html")
	public ModelAndView addAdminSuccess(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/Administrator_/MainPage");
		
		System.out.println("Adding Administrator");
		
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		
		System.out.println("Name: " + userName + " password: " + userPassword);
		
		Administrator administrator = new Administrator();
		administrator.setUserName(userName);
		administrator.setUserPassword(userPassword);
		
		try {
			
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(administrator);
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
			return new ModelAndView("/Administrator_/AddAdmin");
		}
		
		
		return modelAndView;
	}
	
}
