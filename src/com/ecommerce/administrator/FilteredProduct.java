package com.ecommerce.administrator;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.ProductInformation;


@Controller
@RequestMapping(value="/administrator")
public class FilteredProduct {
	
	@RequestMapping(value="/showsearchresults")
	public ModelAndView show(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/Administrator_/FilteredProducts");
		
		String productName = request.getParameter("searchKeyword");
		System.out.println("Product Name: " + productName);
		
		ArrayList<ProductInformation> filteredProduct = new SearchResult().searchResults(productName);
			
		modelAndView.addObject("productDatabaseList", filteredProduct);
		
		return modelAndView;
	}

}
