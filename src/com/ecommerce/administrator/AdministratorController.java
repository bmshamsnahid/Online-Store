package com.ecommerce.administrator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.Session;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.Administrator;
import com.ecommerce.model.ProductInformation;
import com.ecommerce.model.User;

@Controller
@RequestMapping(value="/administrator")
public class AdministratorController {
	
	private List<Administrator> administratorDatabase = null;
	private boolean authenticationChecker = false;
	
	@RequestMapping(value="/")
	public ModelAndView getAdministratorLoginForm() {
		return new ModelAndView("/Administrator_/Administrator");
	}
	
	@RequestMapping(value="/home")
	public ModelAndView administratorMainPage() {
		return new ModelAndView("/Administrator_/MainPage");
	}
	
	@RequestMapping(value="/loginFormSuccess.html")
	public ModelAndView goToMainPage(@ModelAttribute("administrator") Administrator administratorInput) {
		System.out.println("UserName: " + administratorInput.getUserName() + " Password: " + administratorInput.getUserPassword());
		try {
			
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive administrator's userName and userPassword from administrator table
			administratorDatabase = session.createQuery("from Administrator").list();
			session.beginTransaction();
			session.close();
			
			System.out.println("Got: " + administratorInput.getUserName() + " " + administratorInput.getUserPassword());
			
			//Compare Users Input userName and userPassword with user Database 
			for(Administrator administratorTemp : administratorDatabase) {
				System.out.println("name: " + administratorTemp.getUserName() + " and Password: " + administratorTemp.getUserPassword());
				if(administratorInput.getUserName().equals(administratorTemp.getUserName()) && administratorInput.getUserPassword().equals(administratorTemp.getUserPassword()) ) {
					authenticationChecker = true;
					System.out.println("Matched");
					break;
				}
			}
			
			if(authenticationChecker) {
				authenticationChecker = false;
				System.out.println("authentication true");
				return new ModelAndView("/Administrator_/MainPage");
			} else {
				System.out.println("authentivation false");
				return new ModelAndView("/Administrator_/Administrator");
			}
			
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
			return new ModelAndView("/Administrator_/Administrator");
		}		
	}
	
	/*@RequestMapping(value="/")
	public ModelAndView mainPage() {
		ModelAndView modelAndView;
		
		modelAndView = new ModelAndView("/Administrator_/MainPage");
		
		return modelAndView;
	} */
	
	@RequestMapping(value="/productOperation.html")
	public ModelAndView productOperation() {
		ModelAndView modelAndView;
		
		modelAndView = new ModelAndView("/Administrator_/CatagoryExposed");
		
		return modelAndView;
	}
}
