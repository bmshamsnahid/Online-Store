package com.ecommerce.administrator;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ecommerce.model.Administrator;

@Controller
@RequestMapping(value="/modifyAdminController")
public class DeleteOrModifyAdministrator {

	@RequestMapping("/removeAdmin.html")
	public ModelAndView removeAdmin(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/Administrator_/RemoveAdmin");
		
		return modelAndView;
	}
	
	@RequestMapping("/removeAdminSuccess")
	public ModelAndView removeAdminSuccess(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("/Administrator_/MainPage");
		
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		
		System.out.println("Name: " + userName + " password: " + userPassword);
		
		Administrator administrator = new Administrator();
		administrator.setUserName(userName);
		administrator.setUserPassword(userPassword);
		
		try {
			
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory();
			org.hibernate.Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(administrator);
			session.getTransaction().commit();
			session.close();
		} catch(Exception e) {
			System.out.println("Exception on storing in database: " + e.toString());
			return new ModelAndView("/Administrator_/RemoveAdmin");
		}
		
		return  modelAndView;
	}
	
}
