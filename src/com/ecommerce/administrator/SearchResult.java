package com.ecommerce.administrator;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ecommerce.model.ProductInformation;

public class SearchResult {
	
	
	
	//Here we get the product name and return the matched arraylist of product Information
	public ArrayList<ProductInformation> searchResults(String productName) {
		ArrayList<ProductInformation> alProductInformation = new ArrayList<>();
		
		List<ProductInformation> productDatabase = null;	//all the product information goes here
		
		try {
			SessionFactory sessionFactory  = new Configuration().configure().buildSessionFactory(); 
			org.hibernate.Session session = sessionFactory.openSession();
			
			//retrive all product information from productInformation table
			productDatabase = session.createQuery("from ProductInformation").list();	//got all the product information
			
			session.beginTransaction();
			session.close();		
		} catch(Exception e) {
			System.out.println("Exception on fetching database: " + e.toString());
		}
		
		//if found a product name theat contains the product name sequence then we add it
		for(ProductInformation pi : productDatabase) {
			if(pi.getProductName().toLowerCase().contains(productName.toLowerCase())) {
				alProductInformation.add(pi);
			}
		}
		
		return alProductInformation;
	}

}
