<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<meta charset="UTF-8">

<title>E commerce Demo</title>
<style>
body {
    background: url('http://i.imgur.com/Eor57Ae.jpg') no-repeat fixed center center;
    background-size: cover;
    font-family: Montserrat;
}

.logo {
    width: 213px;
    height: 36px;
    /* background: url('http://i.imgur.com/fd8Lcso.png') no-repeat; */
    margin: 30px auto;
}

.login-block {
    width: 320px;
    padding: 20px;
    background: #fff;
    border-radius: 5px;
    border-top: 5px solid #ff656c;
    margin: 0 auto;
}

.login-block h1 {
    text-align: center;
    color: #000;
    font-size: 18px;
    text-transform: uppercase;
    margin-top: 0;
    margin-bottom: 20px;
}

.login-block input {
    width: 100%;
    height: 42px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    font-size: 14px;
    font-family: Montserrat;
    padding: 0 20px 0 50px;
    outline: none;
}

.login-block input#username {
    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px top no-repeat;
    background-size: 16px 80px;
}

.login-block input#username:focus {
    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px bottom no-repeat;
    background-size: 16px 80px;
}

.login-block input#password {
    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px top no-repeat;
    background-size: 16px 80px;
}

.login-block input#password:focus {
    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px bottom no-repeat;
    background-size: 16px 80px;
}

.login-block input:active, .login-block input:focus {
    border: 1px solid #ff656c;
}

.login-block button {
    width: 100%;
    height: 40px;
    background: #ff656c;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #e15960;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
}

.login-block button:hover {
    background: #ff7b81;
}

</style>
</head>

<body>


<div class="logo"></div>
<h2 align="center" >E Commerce Demo</h2>
<div class="login-block">
    <h1>Register</h1>
    <form action="/E-CommerceDemo/registrationController/registrationComplete.html">
    	<input type="email" name="userEmail" value="" placeholder="Email" id="username" />
    	<input type="text" name="userFullName" value="" placeholder="Full Name" id="username" />
    	<input type="text" name="userName" value="" placeholder="User Name" id="username" />
    	<input type="text" name="userDateOfBirth" placeholder="YYYY-MM-DD" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Enter a date in this formart YYYY-MM-DD"/>
    	<input type="text" name="userSex" value="" placeholder="Sex" id="username" />
    	<input type="text" name="userMobileNumber" value="" placeholder="Cell No." id="username" />
    	<input type="text" name="userPreferredContactMethod" value="" placeholder="Contact Method" id="username" />
    	<input type="text" name="userCountry" value="" placeholder="Country" id="username" />
    	<input type="password" name="userPassword" value="" placeholder="Password" id="password" />
    	<input type="password" name="userConfirmPassword" value="" placeholder="Confirm Password" id="password" />
    	<button>Submit</button>
    </form>
</div>
</body>

</html>