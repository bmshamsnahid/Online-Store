<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<%@page import="java.io.File"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="javax.imageio.ImageIO"%>
    <%@page import="java.io.ByteArrayOutputStream"%>

    <%@page import="java.math.BigInteger"%>
    <%@page import="javax.xml.bind.DatatypeConverter"%>
    <%@page import="java.awt.image.BufferedImage"%>
    
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Tangerine">
     
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
    
    
    <script type="text/javascript">
		$( document ).ready(function() {
			$("#spanNotAvailable").hide();
			$("#spanAlreadyAdded").hide();
			var stock = ${productInformation.stock};
			console.log("var is " + stock);
			
			var cartExistence = ${cartExistence};	//checking if already added to the cart
			console.log("Existence of the product is: " + cartExistence);
			if(cartExistence == true) {
				$("#spanAddToCart").hide();
				$("#spanAlreadyAdded").show();
			} 
			if (stock == 0) {
				$("#spanAddToCart").hide();
				$("#spanNotAvailable").show();
			}
		});
	</script>
    
    <style type="text/css">
    	.link {
    		font-size: 30px;
    		font-weight: bold;
    		background-color: #ebebe0;
    		color: #000000;
    		padding: 7px;
    		border-radius: 25px;
    		
    	}
    	.name {
    		font-size: 45px;
    		font-weight: bold;
    		font-style: italic;
    	}
    	.link:hover {
		    font-size: 35px;
    		font-weight: bold;
    		background-color: #ccccb3;
    		color: #ffffff;
    		padding: 2px;
    		border-radius: 25px;
		}
		
		.notAvailable {
			font-size: 30px;
    		font-weight: bold;
    		background-color: red;
    		color: #000000;
    		padding: 7px;
    		border-radius: 25px;
		}
		.alreadyAdded {
			font-size: 30px;
    		font-weight: bold;
    		background-color: orange;
    		color: #000000;
    		padding: 7px;
    		border-radius: 25px;
		}
		
		.fontTangarine {
	        font-family: 'Tangerine';
	        font-size: 48px;
	    }
		
    </style>  
		
</head>

<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="UserHeader.jsp" %>    

    <!-- Page Content -->
    <div class="container">
	
	<div class='row'>
		<div class='col-md-12'>'
			<h3 style="font-family: 'Kumar One', cursive; font-size: 48px;">${productInformation.productName}</h3>
			<h1 style="font-family: 'Lobster', cursive; font-size: 25px; font-style: bold;">${productInformation.productShortInformation}</h1>
		</div>
		
	</div>
		
    <div class="row">
         
         <div class="col-md-12">
			<c:set var="productName">${productInformation.productName}</c:set>
			<%
			try{
			  String imgName="/home/bmshamsnahid/ECommerceDemoImages/" + (String)pageContext.getAttribute("productName") + ".jpg";
			  BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
			    ByteArrayOutputStream baos = new ByteArrayOutputStream();
			    ImageIO.write( bImage, "jpg", baos );
			    baos.flush();
			    byte[] imageInByteArray = baos.toByteArray();
			    baos.close();                                   
			    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
			    %>
			<img src="data:image/jpg;base64, <%=b64%>" class="img-responsive center-block" style="width:650px;height:400px; border-radius: 16px;" />                            
			<% 
			}catch(IOException e){
			  System.out.println("Error: " + "${item.productName}" + ".jpg" +e);
			}
			%>
          </div>
     </div>
     
     <br/>
     
     <div class="col-md-12">
		<span class="pull-right" id="spanAddToCart">
			<a href="/E-CommerceDemo/product/newWishList.html?productId=${productInformation.productId}" class='' style="font-family: 'Coda Caption', sans-serif; color: green; font-size: 25px;">Add to my cart</a>
		</span>
	</div>
	<div class="col-md-12">
		<span class="pull-right" id="spanNotAvailable">
			<h3 class='' style="font-family: 'Coda Caption', sans-serif; color: red; font-size: 25px;" >Not Available</h3>
		</span>
	</div>
	<div class="col-md-12">
		<span class="pull-right" id="spanAlreadyAdded">
			<h3 class='' style="font-family: 'Coda Caption', sans-serif; color: #b37700; font-size: 25px;" >Already Added</h3>
		</span>
	</div>
        
     <h2 style="font-family: 'Passion One', cursive; font-size: 40px;">Full Information</h2>
     <blockquote class="blockquote">
	    <p class="mb-0" style="font-family: 'Francois One', sans-serif;">
	    	${productInformation.productFullInformation} 
	    </p>
	 </blockquote>

    </div>
    <!-- /.container -->
    <!-- Start of comment display section html -->
    <h3 style="font-family: 'Passion One', cursive; font-size: 40px;">Product Reviews</h3>
    <c:forEach items="${productReviews}" var="item">
    	<%-- <h2>${item.productReview}</h2> --%>
    
    	<div class="container">
		    <div class="row" >
		        <div class="col-sm-12">
		            <div class="panel panel-white post panel-shadow" style="background-color:#ebebe0;">
		                <div class="post-heading">
		                    <div class="pull-left image">
		                        <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-rounded avatar" alt="user profile image">
		                    </div>
		                    <div class="pull-left meta">
		                        <div class="title h5">
		                           <b>${item.userName}</b>
		                        </div>
		                    </div>
		                </div> 
		                <div class="post-description"> 
		                    <p style="font-family: 'Sriracha', cursive; font-size: 30px;" >${item.productReview}</p>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
    </c:forEach>
    
	
	<!-- End of comment display section html -->
	<div class="container-fluid jumbotron">
		<h3 class="text-center" style="font-family: 'Archivo Black', sans-serif; font-size: 25px;">Your Review</h3>
		<form class="form-horizontal" action="/E-CommerceDemo/product/addReview?productId=${productInformation.productId}"> 
		  <div class="form-group">
		    <div class="col-sm-12">
		    	<input type="hidden" class="form-control" id="Id" name="productId" value="${productInformation.productId}">
		      	<%-- <input type="text" class="form-control" id="Review" name="productReview" placeholder="Your Review"> --%>
		      	<textarea class="form-control" rows="5" id="Review" name="productReview" style="font-family: 'Sriracha', cursive; font-size: 20px;"></textarea>
		    </div>
		  </div>
		  
		  <div class="form-group"> 
		    <div class="col-sm-offset-10">
		      <button type="submit" class="btn btn-default">Submit</button>
		    </div>
		  </div>
		</form>
	</div>
	
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ include file="UserFooter.jsp" %>
</body>

</html>
