<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<%@page import="java.io.File"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="javax.imageio.ImageIO"%>
    <%@page import="java.io.ByteArrayOutputStream"%>

    <%@page import="java.math.BigInteger"%>
    <%@page import="javax.xml.bind.DatatypeConverter"%>
    <%@page import="java.awt.image.BufferedImage"%>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
	
	
</head>

<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="UserHeader.jsp" %>
    

    <!-- Page Content -->
    
    <div class="container">

        <div class="row">
		<h2><b> <font color="white">${catagoryName}</b></h2>
            
            <div class="col-md-12">

                <div class="row">
                	<c:forEach items="${productCatagoryItemList}" var="item">
					   <div class="col-sm-3 col-lg-3 col-md-3">
	                        <div class="thumbnail" >
	                        	
	                            <c:set var="productName">${item.productName}</c:set>
	                            <%
							    //write image
							    try{
							      String imgName="/home/bmshamsnahid/ECommerceDemoImages/" + (String)pageContext.getAttribute("productName") + ".jpg";
							      BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
							        ByteArrayOutputStream baos = new ByteArrayOutputStream();
							        ImageIO.write( bImage, "jpg", baos );
							        baos.flush();
							        byte[] imageInByteArray = baos.toByteArray();
							        baos.close();                                   
							        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
							        %>
							        <a href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}">
							        	<img src="data:image/jpg;base64, <%=b64%>" class="img-rounded" style="width:360px;height:175px;" />                            
							        </a>
							        <% 
							    }catch(IOException e){
							      System.out.println("Error: " + "${item.productName}" + ".jpg" +e);
							    }
							    %>
	                            <div class="caption productDisplay">
	                                 <h4 class="pull-right" style="font-family: 'Courgette', cursive; font-size: 15px;">BDT ${item.productPrice} </h4>
									<h4><a href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}" style="font-family: 'Bitter', serif; font-size: 20px">${item.productName}</a></h4>    
	                            </div>
	                            
	                        </div>
	                    </div>
					</c:forEach>
                
                </div>

            </div>

        </div>

    </div>
    
    <!-- /.container -->

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ include file="UserFooter.jsp" %>
    <!-- /.container -->

</body>

</html>
