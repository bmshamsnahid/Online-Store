<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<%@page import="java.io.File"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="javax.imageio.ImageIO"%>
    <%@page import="java.io.ByteArrayOutputStream"%>

    <%@page import="java.math.BigInteger"%>
    <%@page import="javax.xml.bind.DatatypeConverter"%>
    <%@page import="java.awt.image.BufferedImage"%>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
	
	<style type="text/css">
		.bb {
			font-weight: bold;
			font-size:  25px;
			color: black;
			padding: 10px;
		}
		.name {
    		font-size: 45px;
    		font-weight: bold;
    		font-style: italic;
    		color: #5c5c3d;
    	}
    	.buttonPm {
		    background-color: #4CAF50; /* Green */
		    border-radius: 12px;
		    color: white;
		    padding-left: 5px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		}
	</style>
	
</head>

<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="UserHeader.jsp" %>
	
    <!-- Page Content -->
    
    <div class="container">

        <div class="row">

            
            <span class='name'>My Cart</span>
            
            <span class='bb'> (Total Amount: BDT ${totalMoney})</span>
            
            
            <div class='row'>
            	<button type="button" class="btn btn-primary disabled" onclick='location.href="#"'>Buy All <span class="badge">${size }</span></button>
	            <button type="button" class="btn btn-danger" onclick='location.href="/E-CommerceDemo/myCartController/removeAll.html"'>Delate All <span class="badge">${size }</span></button>
            </div>
            
            <br/>
            <br/>
            
            <div class="col-md-12">

                <div class="row">
                
	               	<c:forEach items="${alProductInformationList}" var="item">
					   <div class="col-sm-3 col-lg-3 col-md-3">
	                        <div class="thumbnail">
	                            <c:set var="productName">${item.productName}</c:set>
	                            <%
							    //write image
							    try{
							    	
							      String imgName="/home/bmshamsnahid/ECommerceDemoImages/" + (String)pageContext.getAttribute("productName") + ".jpg";
							      BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
							        ByteArrayOutputStream baos = new ByteArrayOutputStream();
							        ImageIO.write( bImage, "jpg", baos );
							        baos.flush();
							        byte[] imageInByteArray = baos.toByteArray();
							        baos.close();                                   
							        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
							        %>
							        <a href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}">
							        	<img src="data:image/jpg;base64, <%=b64%>" class="img-rounded" onmouseover="displayInformation(${item.productName})" style="width:360px;height:175px;" />                            
							        </a>
							        <%
							    }catch(IOException e){
							      System.out.println("Error: " + "${item.productName}" + ".jpg" +e);
							    } 
							    %>
	                                                        
	                            <div class="caption ">
	                            	
	                            
	                            	<div class='row col-md-12' >
	                            		<div class="dropdown col-md-6">
										  <button type="button" class="btn btn-primary" onclick='location.href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}"'>${item.productName}</button>
										  <%-- <button type="button" class="btn btn-primary" onclick='location.href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}'>${item.productName}</button> --%>
										</div>
										
										<div class="dropdown col-md-6">
										  <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Action
										  <span class="caret"></span></button>
										  <ul class="dropdown-menu">
										  		<button type="button" class="btn btn-primary disabled" onclick='location.href="#"'>Buy</button>
											    <button type="button" class="btn btn-primary btn-danger" onclick='location.href="/E-CommerceDemo/myCartController/removeProduct.html?productId=${item.productId}"'>Remove </button>
										  </ul>
										</div>                         		
	                            	</div>
	                            
	                                <div class='row'>
	                                	<h4 class="" style=" font-family: 'Courgette', cursive; padding-right: 5px; font-size: 20px; text-align:center"> <br/>Price: BDT ${item.productPrice}</h4>
	                                </div>
	                                
	                            </div>
	                            
	                            
	                        </div>
	                    </div>
					</c:forEach>
                
                </div>

            </div>

        </div>

    </div>
    
    
    
    <!-- /.container -->

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ include file="UserFooter.jsp" %>
    <!-- /.container -->

</body>

</html>
