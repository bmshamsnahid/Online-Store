<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
	
	<style>
		body{
		    background: url('http://i.imgur.com/Eor57Ae.jpg') no-repeat fixed center center;
		    padding:50px;
		}
		
		#login-dp{
		    min-width: 250px;
		    padding: 14px 14px 0;
		    overflow:hidden;
		    background-color:rgba(255,255,255,.8);
		}
		#login-dp .help-block{
		    font-size:12px    
		}
		#login-dp .bottom{
		    background-color:rgba(255,255,255,.8);
		    border-top:1px solid #ddd;
		    clear:both;
		    padding:14px;
		}
		#login-dp .social-buttons{
		    margin:12px 0    
		}
		#login-dp .social-buttons a{
		    width: 49%;
		}
		#login-dp .form-group {
		    margin-bottom: 10px;
		}
		.btn-fb{
		    color: #fff;
		    background-color:#3b5998;
		}
		.btn-fb:hover{
		    color: #fff;
		    background-color:#496ebc 
		}
		.btn-tw{
		    color: #fff;
		    background-color:#55acee;
		}
		.btn-tw:hover{
		    color: #fff;
		    background-color:#59b5fa;
		}
		@media(max-width:768px){
		    #login-dp{
		        background-color: inherit;
		        color: #fff;
		    }
		    #login-dp .bottom{
		        background-color: inherit;
		        border-top:0 none;
		    }
		}
	</style>
</head>

<body>
	
    <!-- Navigation Start -->
    	
    	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="http://localhost:8080/E-CommerceDemo/">Store Home</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#">Products</a></li>
		        <li><a href="#">Category</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Customize <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#">Language</a></li>
		            <li><a href="#">You Here !!!</a></li>
		            <li><a href="#">Contact Us</a></li>
		            <li class="divider"></li>
		            <li><a href="#">About Us</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Feedback</a></li>
		          </ul>
		        </li>
		      </ul>
		      <form class="navbar-form navbar-left" role="search">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search">
		        </div>
		        <button type="submit" class="btn btn-default">Submit</button>
		      </form>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="active"><a href="#">My Carts</a></li>
		        <li> <a href="http://localhost:8080/E-CommerceDemo/product/addToWishList.html?productName=${productInformation.productName}">Add to wishlist</a></li>		        
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
    
    <!-- Navigation End -->
    

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">E commerce Shop Name</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">${productInformation.productCatagory}</a>
                    <a href="#" class="list-group-item">Review</a>
                    <a href="#" class="list-group-item">Rate</a>
                    <a href="#" class="list-group-item">Complain</a>
                </div>
                <p><font size="3" color="#D3D3D3"><b><i>${productInformation.productShortInformation}</i></b></font></p>
                <p><font size="5" color="#D3D3D3"><b><i>Price: ${productInformation.productPrice}</i></b></font></p>
            </div>
            <div class="col-md-9">
                	<p><font size="10" color="white"><b>${productInformation.productName}</b></p>
            </div>
            
            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                         <img src="http://placehold.it/800x300" class="img-rounded" alt="Cinque Terre">
                    </div>

                </div>
                
                <p><font size="4" color="#D3D3D3"><b>Info:</b><i>Product Full Information goes her asdhka asdasd masdas aksdbvas asdbaskd sadasd jasdhf ajklshd e, al a customer need to know about the product</i></font></p>
                <br/>

                <div class="row">
                
	               	<div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                        </div>
                    </div>
                
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Shams Inc.</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
