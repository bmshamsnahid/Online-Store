<html>
<head>

</head>
	
<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="AdministratorHeader.jsp" %>
	
	<br/>
	<br/>
	
	<table class="table table-borderless">
		<tr>
			<th class="text-center"><h1>Admin Operations</h1></th>
		</tr>
		<tr>
			<td class="text-center"><a href="/E-CommerceDemo/addAdministratorController/addAdministrator.html">Add Admin</a></td>
		</tr>
		<tr>
			<td class="text-center"><a href="/E-CommerceDemo/modifyAdminController/removeAdmin.html">Remove Admin</a></td>
		</tr>
		<tr>
			<td class="text-center"><a href="/E-CommerceDemo/addProductController/addProduct.html">Add Product</a></td>
		</tr>
		<tr>
			<td class="text-center"><a href="/E-CommerceDemo/deleteOrModify/show">Remove/Modify Product</a></td>
		</tr>
	</table>

</body>
	
</html>