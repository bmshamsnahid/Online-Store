<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
	<title></title>
	<style type="text/css">
		li:hover { 
    		cursor: pointer;
		}
	</style>
	
</head>

<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="AdministratorHeader.jsp" %>
	
	<br/>
	<br/>
	<br/>
	
	<div class='login-block' style="width: 800px;">
			<h1 class='text-center' >New Product Information</h1>
			<form action="/E-CommerceDemo/addProductController/addingProductSuccess.html" enctype="multipart/form-data">
				<input class='' type="text" name="productName" value="" placeholder="Name" />
				<input class='' type="text" name="productPrice" value="" placeholder="Price" />
				<input class='' type="text" name="productCatagory" id="categoryInput" value="" placeholder="New Category" />
					
					<div class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-family: 'Fjalla One', sans-serif; font-size: 25px;">Existing Category <b class="caret"></b></a> 
			          <ul class="dropdown-menu">
			          	<c:forEach items="${productCatagoryList}" var="item">
	   						<li ><a onclick="myFunction(this)" id="item" style="font-family: 'Fjalla One', sans-serif; font-size: 20px;">${item}</a></li>
						</c:forEach>
			          </ul>
			        </div>
			        <br/>
			        
				<input class='' type="text" name="productShortInformation" value="" placeholder="Short Information" />
				<input class='' type="text" name="productFullInformation" value="" placeholder="Full Information" />
				<input class='' type="text" name="stock" value="" placeholder="Stock" />
				
				<input class="form-control" type="file" id="myFile" name="productImage" placeholder="Image" />
				
				<button type='submit'>Add Information</button>
			</form>			
		</div>
	
	<script>
		function myFunction(param) {
		    document.getElementById("categoryInput").value = (param.innerHTML).trim();
		}
	</script>

</body>
</html>