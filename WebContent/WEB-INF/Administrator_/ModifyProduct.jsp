<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
	<title></title>
	<style type="text/css">
		li:hover { 
    		cursor: pointer;
		}
	</style>
	
</head>

<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="AdministratorHeader.jsp" %>
	
	<br/>
	<br/>
	<br/>
	
	Size: ${size }
	
	<div class='login-block' style="width: 800px;">
			<h1 class='text-center' >New Product Information</h1>
			<form action="/E-CommerceDemo/deleteOrModify/modifyProductSuccess.html" enctype="multipart/form-data">
				<input class='' type="hidden" name="productId" value = "${productInformation.productId}" />
				Product Name: <input class='' type="text" name="productName" value = "${productInformation.productName}" />
				Product Price: <input class='' type="text" name="productPrice" value = "${productInformation.productPrice}" />
				Product Category: <input class='' type="text" id="categoryInput" name="productCatagory" value = "${productInformation.productCatagory}" />
					
					<div class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-family: 'Fjalla One', sans-serif; font-size: 25px;">Existing Category <b class="caret"></b></a> 
			          <ul class="dropdown-menu">
			          	<c:forEach items="${productCatagoryList}" var="item">
	   						<li ><a onclick="myFunction(this)" id="item" style="font-family: 'Fjalla One', sans-serif; font-size: 20px;">${item}</a></li>
						</c:forEach>
						<li> <a onclick="myFunction(this)" id="item">Bal</a> </li>
						<li ><a onclick="myFunction(this)" id="item">Sal</a></li>
						<li ><a onclick="myFunction(this)" id="item">Bal</a></li>
			          </ul>
			        </div>
			        <br/>
				
				Product Short Information: <input type="text" name="productShortInformation" value = "${productInformation.productShortInformation}" />
				Product Full Information: <input class='' type="text" name="productFullInformation" value = "${productInformation.productFullInformation}" />
				Product Stock: <input class='' type="text" name="productStock" value = "${productInformation.stock}" />
				
				<button type='submit'>Update Product</button>
			</form>			
		</div>
	
	<script>
		function myFunction(param) {
		    document.getElementById("categoryInput").value = (param.innerHTML).trim();
		}
	</script>

</body>
</html>