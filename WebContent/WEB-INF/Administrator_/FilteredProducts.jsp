<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
	<title>Products Modify</title>
</head>

<body>
	
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="AdministratorHeader.jsp" %>
	
	<br/>
	<br/>
	
	<h1 class='text-center'>Product Information</h1>
	
	<div class="container"> 
		<table class="table table-condensed table-striped">
			<tr>
				<th>Product Name</th>
				<th>Product Category</th>
				<th>Product Price</th>
				<th>Product Stocks</th>
				<th>Product Views</th>
				<th>Manage</th>
			</tr>
			<c:forEach items="${productDatabaseList}" var="item">
				<tr>			
					<td>${item.productName}</td>
					<td>${item.productCatagory}</td>
					<td>${item.productPrice}</td>
					<td>${item.stock}</td>
					<td>${item.hit}</td>
					<td>
						<div class="dropdown">
							  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Manage Product
							  <span class="caret"></span></button>
							  <ul class="dropdown-menu">
							  		<button type="button" class="btn btn-primary" onclick='location.href="/E-CommerceDemo/deleteOrModify/modifyProduct.html?productName=${item.productName}"'>Modify</button>
								    <button type="button" class="btn btn-primary btn-danger" onclick='location.href="/E-CommerceDemo/deleteOrModify/deleteProduct?productName=${item.productName}"'>Delete </button>
							  </ul>
						</div>
					</td> 
				</tr>
			</c:forEach>
		</table>
	</div>
	
</body>

</html>
