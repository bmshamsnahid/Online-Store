<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<style type="text/css">
		.boldBig {
			font-weight: bold;
			color: white;
		}
		body {
		    background-color: #ccccb3;
		    background-size: cover;
		    font-family: Montserrat;
		}
		
		.logo {
		    width: 213px;
		    height: 36px;
		    /* background: url('http://i.imgur.com/fd8Lcso.png') no-repeat; */
		    margin: 30px auto;
		}
		
		.login-block {
		    width: 320px;
		    padding: 20px;
		    background: #fff;
		    border-radius: 5px;
		    border-top: 5px solid #ff656c;
		    margin: 0 auto;
		}
		
		.login-block h1 {
		    text-align: center;
		    color: #000;
		    font-size: 18px;
		    text-transform: uppercase;
		    margin-top: 0;
		    margin-bottom: 20px;
		}
		
		.login-block input {
		    width: 100%;
		    height: 42px;
		    box-sizing: border-box;
		    border-radius: 5px;
		    border: 1px solid #ccc;
		    margin-bottom: 20px;
		    font-size: 14px;
		    font-family: Montserrat;
		    padding: 0 20px 0 50px;
		    outline: none;
		}
		
		.login-block input#username {
		    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px top no-repeat;
		    background-size: 16px 80px;
		}
		
		.login-block input#username:focus {
		    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px bottom no-repeat;
		    background-size: 16px 80px;
		}
		
		.login-block input#password {
		    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px top no-repeat;
		    background-size: 16px 80px;
		}
		
		.login-block input#password:focus {
		    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px bottom no-repeat;
		    background-size: 16px 80px;
		}
		
		.login-block input:active, .login-block input:focus {
		    border: 1px solid #ff656c;
		}
		
		.login-block button {
		    width: 100%;
		    height: 40px;
		    background: #ff656c;
		    box-sizing: border-box;
		    border-radius: 5px;
		    border: 1px solid #e15960;
		    color: #fff;
		    font-weight: bold;
		    text-transform: uppercase;
		    font-size: 14px;
		    font-family: Montserrat;
		    outline: none;
		    cursor: pointer;
		}
		
		.login-block button:hover {
		    background: #ff7b81;
		}
		
	</style>

</head>

<body>
	
    <!-- Navigation Start -->
    	
    	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand boldBig" href="/E-CommerceDemo/administrator/home">Admin Home</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li><a class='boldBig' href="/E-CommerceDemo/addProductController/addProduct.html">Add Product</a></li>
		        <li><a class='boldBig' href="/E-CommerceDemo/deleteOrModify/show">Manage Product</a></li>
		        <form class="navbar-form navbar-left" role="search" action="/E-CommerceDemo/administrator/showsearchresults">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search" name="searchKeyword">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			     </form>
		      </ul>
		      
		      <ul class="nav navbar-nav navbar-right">
		        <li> <a class='boldBig' href="/E-CommerceDemo/addAdministratorController/addAdministrator.html">Add Admin</a></li>
		    	<li><a class='boldBig' href="/E-CommerceDemo/modifyAdminController/removeAdmin.html">Remove Admin</a></li>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
    
    <!-- Navigation End -->
</body>