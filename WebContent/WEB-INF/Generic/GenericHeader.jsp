<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<%@page import="java.io.File"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="javax.imageio.ImageIO"%>
    <%@page import="java.io.ByteArrayOutputStream"%>

    <%@page import="java.math.BigInteger"%>
    <%@page import="javax.xml.bind.DatatypeConverter"%>
    <%@page import="java.awt.image.BufferedImage"%>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
	
	<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Anton|Archivo+Black|Baloo+Thambi|Dancing+Script|Fjalla+One|Francois+One|Hind|Indie+Flower|Kumar+One|Libre+Baskerville|Lobster|Montserrat|Passion+One|Signika" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Anton|Fjalla+One|Fugaz+One|Lobster|Oleo+Script|Patua+One" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Bitter|Courgette|Lato" rel="stylesheet">
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
	
	<style>
	 /*
	 Comment section satrt css 
	 */
		
	.panel-shadow {
	    box-shadow: rgba(0, 0, 0, 0.3) 7px 7px 7px;
	}
	.panel-white {
	  border: 1px solid #dddddd;
	}
	.panel-white  .panel-heading {
	  color: #333;
	  background-color: #fff;
	  border-color: #ddd;
	}
	.panel-white  .panel-footer {
	  background-color: #fff;
	  border-color: #ddd;
	}
	
	.post .post-heading {
	  height: 95px;
	  padding: 20px 15px;
	}
	.post .post-heading .avatar {
	  width: 60px;
	  height: 60px;
	  display: block;
	  margin-right: 15px;
	}
	.post .post-heading .meta .title {
	  margin-bottom: 0;
	}
	.post .post-heading .meta .title a {
	  color: black;
	}
	.post .post-heading .meta .title a:hover {
	  color: #aaaaaa;
	}
	.post .post-heading .meta .time {
	  margin-top: 8px;
	  color: #999;
	}
	.post .post-image .image {
	  width: 100%;
	  height: auto;
	}
	.post .post-description {
	  padding: 15px;
	}
	.post .post-description p {
	  font-size: 14px;
	}
	.post .post-description .stats {
	  margin-top: 20px;
	}
	.post .post-description .stats .stat-item {
	  display: inline-block;
	  margin-right: 15px;
	}
	.post .post-description .stats .stat-item .icon {
	  margin-right: 8px;
	}
	.post .post-footer {
	  border-top: 1px solid #ddd;
	  padding: 15px;
	}
	.post .post-footer .input-group-addon a {
	  color: #454545;
	}
	.post .post-footer .comments-list {
	  padding: 0;
	  margin-top: 20px;
	  list-style-type: none;
	}
	.post .post-footer .comments-list .comment {
	  display: block;
	  width: 100%;
	  margin: 20px 0;
	}
	.post .post-footer .comments-list .comment .avatar {
	  width: 35px;
	  height: 35px;
	}
	.post .post-footer .comments-list .comment .comment-heading {
	  display: block;
	  width: 100%;
	}
	.post .post-footer .comments-list .comment .comment-heading .user {
	  font-size: 14px;
	  font-weight: bold;
	  display: inline;
	  margin-top: 0;
	  margin-right: 10px;
	}
	.post .post-footer .comments-list .comment .comment-heading .time {
	  font-size: 12px;
	  color: #aaa;
	  margin-top: 0;
	  display: inline;
	}
	.post .post-footer .comments-list .comment .comment-body {
	  margin-left: 50px;
	}
	.post .post-footer .comments-list .comment > .comments-list {
	  margin-left: 50px;
	}
	 /* 
	 	Comment section end css
	 */
	 
	 
		body{
		    /* background: url('http://i.imgur.com/Eor57Ae.jpg') no-repeat fixed center center; */
		    padding:50px;
		    background-color: #ccccb3;
		}
		
		#login-dp{
		    min-width: 250px;
		    padding: 14px 14px 0;
		    overflow:hidden;
		    background-color:rgba(255,255,255,.8);
		}
		#login-dp .help-block{
		    font-size:12px    
		}
		#login-dp .bottom{
		    background-color:rgba(255,255,255,.8);
		    border-top:1px solid #ddd;
		    clear:both;
		    padding:14px;
		}
		#login-dp .social-buttons{
		    margin:12px 0    
		}
		#login-dp .social-buttons a{
		    width: 49%;
		}
		#login-dp .form-group {
		    margin-bottom: 10px;
		}
		@media(max-width:768px){
		    #login-dp{
		        background-color: inherit;
		        color: #fff;
		    }
		    #login-dp .bottom{
		        background-color: inherit;
		        border-top:0 none;
		    }
		}
	</style>
</head>

<body>
	
    <!-- Navigation Start -->
    	
    	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/E-CommerceDemo/" style="font-family: 'Anton', sans-serif; font-size: 30px;">Home</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li ><a href="/E-CommerceDemo/productsDisplay/display.html" style="font-family: 'Fjalla One', sans-serif; font-size: 25px;">Products</a></li>
		        <li><a href="/E-CommerceDemo//productsDisplay/categories.html" style="font-family: 'Fjalla One', sans-serif; font-size: 25px;">View</a></li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-family: 'Fjalla One', sans-serif; font-size: 25px;">Category <b class="caret"></b></a> 
		          <ul class="dropdown-menu">
		          	<c:forEach items="${productCatagoryList}" var="item">
   						<li ><a href="/E-CommerceDemo/CatagoryController/Exposed?productCatagory=${item}" style="font-family: 'Fjalla One', sans-serif; font-size: 20px;">${item}</a></li>
					</c:forEach>
		          </ul>
		        </li>
		        
		      </ul>
		      <form class="navbar-form navbar-left" role="search" action="/E-CommerceDemo/genericController/search.html">
		        <div class="form-group" style="font-family: 'Oleo Script', cursive; ">
		          <input type="text" class="form-control" placeholder="Search" name="searchKeyword" style="font-size: 25px;">
		        </div>
		        <button type="submit" class="btn btn-default" style=" font-family: 'Oleo Script', cursive; font-size: 15px;">Submit</button>
		      </form>
		      <ul class="nav navbar-nav navbar-right">
		        <li><p class="navbar-text">Already have an account?</p></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-family: 'Fjalla One', sans-serif; font-size: 25px;">Sign In<span class="caret"></span></a>
					<ul id="login-dp" class="dropdown-menu">
						<li>
							 <div class="row">
									<div class="col-md-12">
										
										 <form class="form" role="form" method="post" accept-charset="UTF-8" id="login-nav" action="/E-CommerceDemo/loginController/loginSuccess.html">
												<div class="form-group">
													 <label class="sr-only" for="exampleInputEmail2" >Email address</label>
													 <input type="text" name="userName" class="form-control" id="exampleInputEmail2" placeholder="Email address" required style="font-family: 'Oleo Script', cursive; font-size: 20px;">
												</div>
												<div class="form-group">
													 <label class="sr-only" for="exampleInputPassword2">Password</label>
													 <input type="password" name="userPassword" class="form-control" id="exampleInputPassword2" placeholder="Password" style="font-family: 'Oleo Script', cursive; font-size: 20px;" required >
		                                             
												</div>
												<div class="form-group">
													 <button type="submit" class="btn btn-primary btn-block" style="font-family: 'Oleo Script', cursive; font-size: 20px;" >Sign in</button>
												</div>												
										 </form>
									</div>
									<div class="bottom text-center">
										<!-- New here ? <a href="/E-CommerceDemo/registrationController/registrationComplete.html"><b>Join Us</b></a> -->
										<a href="/E-CommerceDemo/registrationController/registrationForm.html" style="font-family: 'Fjalla One', sans-serif; font-size: 20px;">Join Us</a>
									</div>
							 </div>
						</li>
					</ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
    
    <!-- Navigation End -->
</body>