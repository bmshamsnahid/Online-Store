<html>
	<head>
		<title>Registration</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		
		<style>
			body {
			    padding:50px;
		    	background-color: #ccccb3;
			    background-size: cover;
			    font-family: Montserrat;
			}
			
			.logo {
			    width: 213px;
			    height: 36px;
			    /* background: url('http://i.imgur.com/fd8Lcso.png') no-repeat; */
			    margin: 30px auto;
			}
			
			.login-block {
			    width: 800px;
			    padding: 20px;
			    backgroun	d: #fff;
			    border-radius: 5px;
			    border-top: 5px solid #ff656c;
			    margin: 0 auto;
			}
			
			.login-block h1 {
			    text-align: center;
			    color: #000;
			    font-size: 18px;
			    text-transform: uppercase;
			    margin-top: 0;
			    margin-bottom: 20px;
			}
			
			.login-block input {
			    width: 100%;
			    height: 42px;
			    box-sizing: border-box;
			    border-radius: 5px;
			    border: 1px solid #ccc;
			    margin-bottom: 20px;
			    font-size: 14px;
			    font-family: Montserrat;
			    padding: 0 20px 0 50px;
			    outline: none;
			}
			
			.login-block input#username {
			    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px top no-repeat;
			    background-size: 16px 80px;
			}
			
			.login-block input#username:focus {
			    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px bottom no-repeat;
			    background-size: 16px 80px;
			}
			
			.login-block input#password {
			    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px top no-repeat;
			    background-size: 16px 80px;
			}
			
			.login-block input#password:focus {
			    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px bottom no-repeat;
			    background-size: 16px 80px;
			}
			
			.login-block input:active, .login-block input:focus {
			    border: 1px solid #ff656c;
			}
			
			.login-block button {
			    width: 100%;
			    height: 40px;
			    background: #ff656c;
			    box-sizing: border-box;
			    border-radius: 5px;
			    border: 1px solid #e15960;
			    color: #fff;
			    font-weight: bold;
			    text-transform: uppercase;
			    font-size: 14px;
			    font-family: Montserrat;
			    outline: none;
			    cursor: pointer;
			}
			
			.login-block button:hover {
			    background: #ff7b81;
			}
			
			</style>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@ include file="GenericHeader.jsp" %>
		
		<div class='login-block'>
			<h1 class='text-center' >Registration</h1>
			<form action="/E-CommerceDemo/recoverPasswordController/recoverComplete.html">
				<div class='container col-md-12'>
					<div class='row'>
						<div class='col-md-6'>
							<input class='' type="email" name="userEmail" value="" placeholder="Email" id="username" />
						</div>
						<div class='col-md-6'>
							<input type="text" name="userFullName" value="" placeholder="Full Name" id="username" />
						</div>
					</div>
					<div class='row'>
						<div class='col-md-6'>
				    		<input type="text" name="userName" value="" placeholder="User Name" id="username" />
						</div>
						<div class='col-md-6'>
							<input type="text" name="userMobileNumber" value="" placeholder="Cell No." id="username" />			
						</div>
					</div>
					<div class='row'>
						<div class='col-md-6'>
							<input type="password" name="userPassword" value="" placeholder="Password" id="password" />
						</div>
						<div class='col-md-6'>
							<input type="password" name="userConfirmPassword" value="" placeholder="Confirm Password" id="password" />
						</div>
				</div>
				<button type='submit'>Submit</button>
			</form>			
		</div>
		<div class='container col-md-12'>
			<span class='text-center'> ${errorLog}</span>
		</div>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@ include file="GenericFooter.jsp" %>
	</body>
</html>