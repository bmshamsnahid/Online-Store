<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<sql:setDataSource var="webappDataSource"
    driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/E-CommerceDemo"
    user="root" password="56316498" />

<sql:query dataSource="${webappDataSource}"
    sql="select id,username from users" var="result" />

<table width="100%" border="1">
    <c:forEach var="row" items="${result.rows}">
        <tr>
            <td>${row.id}</td>
            <td>${row.username}</td>
            <td>
               <img src="${pageContext.servletContext.contextPath }/photoServlet?id=${row.id}" />
            </td>
        </tr>
    </c:forEach>
</table>