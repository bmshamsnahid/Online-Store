<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<%@page import="java.io.File"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="javax.imageio.ImageIO"%>
    <%@page import="java.io.ByteArrayOutputStream"%>

    <%@page import="java.math.BigInteger"%>
    <%@page import="javax.xml.bind.DatatypeConverter"%>
    <%@page import="java.awt.image.BufferedImage"%>
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
    
    <style type="text/css">
    	.cssText { 
    		color: #7c795d; 
    		font-family: 'Trocchi', serif; 
    		font-size: 30px; 
    		font-weight: normal; 
    		line-height: 48px; 
    		margin: 0; 
    	}
    	
    	.seeMore { color: #bf8040; text-decoration: none; text-align: right; padding: 15px; font-size: 30px; text-decoration: underline; }

		.seeMore:hover { color: #da9650; text-decoration: blink; }
    </style>
	
	
</head>

<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ include file="GuestHeader.jsp" %>
    
    <br/>
    <br/>
    <div class="container">
        <div class="col-md-12">
        	
        	<c:forEach items="${productCatagoryList}" var="category">
        		<div class='cssText' >${ category }</div>
        		<hr/>
        		<div class="row">
        			
        			<c:forEach items="${productCategory[category]}" var="item">
        				<div class="col-sm-3 col-lg-3 col-md-3">
        					<div class="thumbnail">
        					<c:set var="productName">${item.productName}</c:set>
                            <%
						    //write image
						    try{
						      String imgName="/home/bmshamsnahid/ECommerceDemoImages/" + (String)pageContext.getAttribute("productName") + ".jpg";
						      BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
						        ByteArrayOutputStream baos = new ByteArrayOutputStream();
						        ImageIO.write( bImage, "jpg", baos );
						        baos.flush();
						        byte[] imageInByteArray = baos.toByteArray();
						        baos.close();                                   
						        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
						        %>
						        <a href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}">
						        	<img src="data:image/jpg;base64, <%=b64%>" class="img-rounded" style="width:360px;height:175px;" />                            
						        </a>
						        <% 
						    }catch(IOException e){
						      System.out.println("Error: " + "${item.productName}" + ".jpg" +e);
						    }
						    %>
                            <div class="caption productDisplay">
                                <h4 class="pull-right" style="font-family: 'Courgette', cursive; font-size: 15px;">BDT ${item.productPrice} </h4>
								<h4><a href="/E-CommerceDemo/product/productPage.html?productName=${item.productName}" style="font-family: 'Bitter', serif; font-size: 20px">${item.productName}</a></h4>
                            </div>
        					</div>
        				</div>
        				
        			</c:forEach>
        			<div class='seeMoreDiv'><a href="/E-CommerceDemo/CatagoryController/Exposed?productCatagory=${ category }" class='seeMore' >See More...</a></div>
        		</div>
        		
        	</c:forEach>
        	
        
        </div>
    </div>
    
	
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ include file="GuestFooter.jsp" %>
    
</body>

</html>
