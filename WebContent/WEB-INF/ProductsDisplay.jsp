<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">

<head>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Commerce Store</title>
	
	<style>
		body{
		    background: url('http://i.imgur.com/Eor57Ae.jpg') no-repeat fixed center center;
		    padding:50px;
		}
		
		#login-dp{
		    min-width: 250px;
		    padding: 14px 14px 0;
		    overflow:hidden;
		    background-color:rgba(255,255,255,.8);
		}
		#login-dp .help-block{
		    font-size:12px    
		}
		#login-dp .bottom{
		    background-color:rgba(255,255,255,.8);
		    border-top:1px solid #ddd;
		    clear:both;
		    padding:14px;
		}
		#login-dp .social-buttons{
		    margin:12px 0    
		}
		#login-dp .social-buttons a{
		    width: 49%;
		}
		#login-dp .form-group {
		    margin-bottom: 10px;
		}
		.btn-fb{
		    color: #fff;
		    background-color:#3b5998;
		}
		.btn-fb:hover{
		    color: #fff;
		    background-color:#496ebc 
		}
		.btn-tw{
		    color: #fff;
		    background-color:#55acee;
		}
		.btn-tw:hover{
		    color: #fff;
		    background-color:#59b5fa;
		}
		@media(max-width:768px){
		    #login-dp{
		        background-color: inherit;
		        color: #fff;
		    }
		    #login-dp .bottom{
		        background-color: inherit;
		        border-top:0 none;
		    }
		}
	</style>
</head>

<body>
	
    <!-- Navigation Start -->
    	
    	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/E-CommerceDemo/">Store Home</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#">Products</a></li>
		        <li ><a href="/E-CommerceDemo">Category</a></li>
		        <li ><a href="/E-CommerceDemo/myCartController/showMyCart.html">My Carts</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Customize <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#">Language</a></li>
		            <li><a href="#">You Here !!!</a></li>
		            <li><a href="#">Contact Us</a></li>
		            <li class="divider"></li>
		            <li><a href="#">About Us</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Feedback</a></li>
		          </ul>
		        </li>
		      </ul>
		      <form class="navbar-form navbar-left" role="search" action="/E-CommerceDemo/genericController/search.html">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search" name="searchKeyword">
		        </div>
		        <button type="submit" class="btn btn-default">Submit</button>
		      </form>
		      <ul class="nav navbar-nav navbar-right">
		        <li ><a href="/E-CommerceDemo/loginController/logOut">Log Out</a></li>		        
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
    
    <!-- Navigation End -->
    

    <!-- Page Content -->
    
    <br />
    
    <div class="container">

        <div class="row">
           
           <div class="col-md-3">
                <div class="list-group">
                    <c:forEach items="${productCatagoryList}" var="item">
   						<a href="http://localhost:8080/E-CommerceDemo/CatagoryController/Exposed?productCatagory=${item}" class="list-group-item">${item}</a>
					</c:forEach>
                </div>
            </div>
            
            <div class="col-md-9">

                <div class="row">
                
	               	<c:forEach items="${productDatabaseList}" var="item">
					   <div class="col-sm-4 col-lg-4 col-md-4">
	                        <div class="thumbnail">
	                            <img src="http://placehold.it/320x150" alt="">
	                            <div class="caption">
	                                <h4 class="pull-right">$ ${item.productPrice}</h4>
	                                <h4><a href="http://localhost:8080/E-CommerceDemo/product/productPage.html?productName=${item.productName}">${item.productName}</a>
	                                </h4>
	                                <p>${item.productShortInformation}</p>
	                            </div>
	                            <div class="ratings">
	                                <p class="pull-right">15 reviews</p>
	                                <p>
	                                    <span class="glyphicon glyphicon-star"></span>
	                                    <span class="glyphicon glyphicon-star"></span>
	                                    <span class="glyphicon glyphicon-star"></span>
	                                    <span class="glyphicon glyphicon-star"></span>
	                                    <span class="glyphicon glyphicon-star"></span>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
					</c:forEach>
                
                </div>

            </div>

        </div>

    </div>
    
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Shams Inc.</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

</body>

</html>
